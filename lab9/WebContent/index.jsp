<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
	
		<link rel="stylesheet" type="text/css" href="css/jquery.mobile-1.3.2.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
		<script type="text/javascript" src="js/jquery.mobile-1.3.2.min.js"></script>
	</head>
	
	<body>
		<div data-role="page" id="home">
		
			<div data-role="header">
				<h1>Home</h1>
			</div>
			
			<!-- Content -->
			<div data-role="content">
				<!-- <h4>A Dialog button:</h4> -->
				<a href="#popup" data-theme="b" data-role="button" data-rel="dialog">Click for Popup Dialog</a>		
			</div>
		
			<div data-role="footer" data-position="fixed">
				<!-- Navigation -->
				<div data-role="navbar">
					<ul>
						<li><a href="#home" class="ui-btn-active">Home</a></li> 
						<li><a href="#slider">Sliders</a></li> 
						<li><a href="#switch">Switch</a></li>
						<li><a href="#autocomplete">Autocomplete</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		
		<div data-role="page" id="slider">
			<div data-role="header">
			<!-- <h1>Slider</h1> -->
			</div>
		
			<div data-role="content">	
				<form>
					<label for="slider-2">Slider</label> 
					<input type="range" name="slider-2" id="slider-2" data-highlight="true" min="0" max="100" value="50">
				</form>	
			</div>
		
			<div data-role="footer" data-position="fixed">
				<!-- Navigation -->
				<div data-role="navbar">
					<ul>
						<li><a href="#home">Home</a></li> 
						<li><a href="#slider" class="ui-btn-active" >Sliders</a></li> 
						<li><a href="#switch">Switch</a></li>
						<li><a href="#autocomplete">Autocomplete</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		
		<div data-role="page" id="switch">
			<div data-role="header">
			<!--<h1>Switch</h1> -->
			</div>
		
			<div data-role="content">	
				<form>
					<label for="flip-2">Flip toggle switch</label> 
					<select name="flip-2" id="flip-2" data-role="slider" data-track-theme="a" data-theme="a">
						<option value="off">Off</option>
						<option value="on">On</option>
					</select>
				</form>		
			</div>
		
			<div data-role="footer" data-position="fixed">
				<!-- Navigation -->
				<div data-role="navbar">
					<ul>
						<li><a href="#home">Home</a></li> 
						<li><a href="#slider">Sliders</a></li> 
						<li><a href="#switch"class="ui-btn-active">Switch</a></li>
						<li><a href="#autocomplete">Autocomplete</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		
		<div data-role="page" id="autocomplete">
			<div data-role="header">
			<!-- <h1>Autocomplete</h1> -->
			</div>
		
			<div data-role="content">	
				<ul data-role="listview" data-filter="true" data-filter-reveal="true" data-filter-placeholder="Search for fruits...">
					<li><a href="#">Apple</a></li> <li><a href="#">Banana</a></li> 
					<li><a href="#">Pear</a></li> <li><a href="#">Grapes</a></li> 
					<li><a href="#">Durian</a></li> <li><a href="#">Mango</a></li> 
					<li><a href="#">Pineapple</a></li> <li><a href="#">Papaya</a></li> 
					<li><a href="#">Mangosteen</a></li> <li><a href="#">Cherry</a></li> 
					<li><a href="#">Watermelon</a></li> <li><a href="#">Jackfruit</a></li> 
					<li><a href="#">Lemon</a></li> <li><a href="#">Lime</a></li> 
					<li><a href="#">Orange</a></li>
				</ul>	
			</div>
		
			<div data-role="footer" data-position="fixed">
				<!-- Navigation -->
				<div data-role="navbar">
					<ul>
						<li><a href="#home">Home</a></li> 
						<li><a href="#slider">Sliders</a></li> 
						<li><a href="#switch">Switch</a></li>
						<li><a href="#autocomplete" class="ui-btn-active">Autocomplete</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div data-role="page" data-close-btn="right" id="popup">
			<div data-role="header">
				<h1>Popup</h1>
			</div>
		
			<div data-role="content">	
				<p>This is a popup dialog</p>		
				
				<p><a href="#" data-role="button" data-rel="back">Close</a></p>	
			</div>
		
			<div data-role="footer" data-position="fixed">
				<h4>Page Footer</h4>
			</div>
			
		</div>

	</body>
</html>