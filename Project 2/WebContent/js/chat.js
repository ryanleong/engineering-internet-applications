// Keep track of messages
index = 0;

$(document).ready(
	function() {
		myGroups();
		// Hide dialog box on load
	    //$("#create").dialog({
	    //    autoOpen: false,
	    //    modal: true
	    //});
		
		// style for button
		$( "input[type=submit], button" ).button();
		
		
		// Share button
		$('#share').click(function(e) {
			e.preventDefault();
			
			// Display dialog on click
			$( "#dialog" ).dialog("open");
				
		});
		
		// hide dialog box
	    $("#deleteGroupPopup").hide();
	    
	    $("#deleteGroup").click(function() {
	    	// Display dialog on click
			$( "#deleteGroupPopup" ).show();
	    });
		
		$( "#emailform" ).submit(function() {
			var chatURL = "url=" + document.URL + "&email=" + $('#email').val();
			
			$.ajax({
				type : "post",
				url : "EmailServlet",
				data : chatURL,
				success : function(msg) {
					
					alert(msg);
					
				}
			});
			
			return false;
		});
		
		$('#chatform').submit(function() {
			var name = userEmail; //$('#name').val();
			var message = $('#message').val();
			var groupID = $('#groupIDHolder').text();
			
			var datastr = 'name=' + name +'&message='+ message + '&groupID=' + $.trim(groupID);
			
			if(message != "" && name != "") {
				$.ajax({
					dataType : 'json', 
					type : "post",
					url : "Chat",
					data : datastr,
					success : function(msg) {
						
						appendToChat(msg);
						
						// increase index
						index = index + 1;
						
						// Clear input box
						$("#message").val("");

						// Set focus
						$("#message").focus();

						// Scroll to bottom
						$("#chat").scrollTop($("#chat")[0].scrollHeight);
					}
				});
			}
			
			return false;
		});
		
		
		// Resize window dynamically
		$(window).resize(
			function() {
				$("#wrapper").height(
						$(window).height() - $("#R3G-navbar").height() - $("#R3G-footer").height() - $("#rj-credits").height() - 50);
	
		});
		
		doPoll();
});

function doPoll(){
	var groupID = $('#groupIDHolder').text();
	var current = "index=" + index + "&groupID=" + $.trim(groupID);
	
	// Initialization
	$.ajax({
		dataType: "json",
		type : "get",
		url : "Chat",
		data : current,
		success : function(msg) {
			
			$.each(msg, function(i, post){
				
				appendToChat(post);
				
				index = post.index;
				
				// Scroll to bottom
				$("#chat").scrollTop($("#chat")[0].scrollHeight);
			});

		}
	});
	
	setTimeout(doPoll, 500);
}

function appendToChat(post) {
	var input = "<span class=\"message\">" + post.message + "</span><br />";
	
	input += "<span class=\"chatdetails\">" + post.datetime + " - " + post.name + "</span><br /><br />";
	
	$("#chat").append(input);
}


