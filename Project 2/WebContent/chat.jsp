<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="com.google.gson.JsonObject" import="java.util.io.*" 
import="javax.servlet.RequestDispatcher"
import="javax.servlet.ServletException"
import="javax.servlet.annotation.WebInitParam"
import="javax.servlet.annotation.WebServlet"
import="javax.servlet.http.HttpServlet"
import="javax.servlet.http.HttpServletRequest"
import="javax.servlet.http.HttpServletResponse" 
import="org.lightcouch.CouchDbClient"
import="org.lightcouch.NoDocumentException"
import="org.lightcouch.CouchDbProperties"
%>
 
<!--  FIND COOKIE INFO -->
<%  CouchDbClient dbClient = new CouchDbClient();
	String access = "R3GISTRA_access";  		//changed cookie name for deliminator
	JsonObject USER = new JsonObject();     //object to use for response
	USER.addProperty("valid", false);
	USER.addProperty("email","");
	javax.servlet.http.Cookie[] cookies = request.getCookies(); 
	
	String userID = "";
	JsonObject userData =null;
	for (int i=0;i<(cookies==null?0:cookies.length);i++){ //checks for null cookies (if zero) 
	javax.servlet.http.Cookie cookie = cookies[i];   
	if(access.equals(cookie.getName())) {
		
		try {
			String token = cookie.getValue().trim();
			String [] tokens = token.split("\\|");
			userID = tokens[0];
			
			//build Json data - maybe used for sessions data
			userData = dbClient.find(JsonObject.class, tokens[0]);
			
			//check if cookie is valid	
			if ((userData.get("accesstoken").getAsString()).equals(tokens[1])){
			    USER.addProperty("valid",true);
			    USER.addProperty("email",tokens[0]);
			    USER.addProperty("accesstoken", tokens[1]);
			} else {
				USER.addProperty("valid",false);
			}
		} catch (NoDocumentException e) {
			// TODO: handle exception
			System.err.println("error user no longer in couchdb");
			} 
		} 
	}

	String userJoinedGroups = null;
	String[] joinedGroups = null;		
	String chatGroupID = null;
	JsonObject group = null;
	String[] allGroupMembers = null;
	String groupOwner = "";
	
	if (USER.get("valid").getAsBoolean() == false) {
		System.out.println("Invalid login");
		
		RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
		RD.forward(request, response);
		return;
	}
	
	// If valid cookie
	else {
		userJoinedGroups = userData.get("joinedGroups").getAsString();
		joinedGroups = userJoinedGroups.split(",");
		
		// Get email and token from confirmation link
		chatGroupID = request.getParameter("chatgroupID");
		USER.addProperty("group",chatGroupID);
		
		if (chatGroupID == null) {
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
		
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
        
		CouchDbClient databaseClient = new CouchDbClient(properties);
		
		// Get group data
		try {
			group = databaseClient.find(JsonObject.class, chatGroupID);

			// Read group owner
			groupOwner = group.get("groupOwner").getAsString();
			
			// Read all group members
			allGroupMembers = group.get("groupMembers").getAsString().split(",");

			
			// Redirect if no members
			if(allGroupMembers == null || allGroupMembers.length == 0) {
				// Redirect
				RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
				RD.forward(request, response);
				return;
			}
			else {
				boolean inGroup = false;
				// Redirect if not part of group
				for(int i = 0; i < allGroupMembers.length; i++) {
					
					if(allGroupMembers[i].trim().equals(userID.trim())) {
						inGroup = true;
						break;
					}
				}
				
				if(!inGroup) {
					System.out.println("Not part of group");
					// Redirect
					RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
					RD.forward(request, response);
					return;
				}
			}
			
		}
		// Check if valid group ID
		catch(NoDocumentException e) {
			System.out.println("No group ID");
			
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
	}

	
	String grouplist = null; 
	CouchDbClient databaseClient = new CouchDbClient();
	CouchDbProperties properties = new CouchDbProperties()
	.setDbName("groups")
	.setCreateDbIfNotExist(true)
	.setProtocol("http")
	.setHost("127.0.0.1").setPort(5984)
	.setMaxConnections(100)
	.setConnectionTimeout(0);

	databaseClient = new CouchDbClient(properties);
	
	// Get all groups user is in
	
	
	if(userJoinedGroups.equals("")) {
		grouplist = "No Available Groups";
	}
	else {
		grouplist = "";
		for(int i = 0; i < joinedGroups.length; i++) {		
			
			try {
				JsonObject groupData = databaseClient.find(JsonObject.class, joinedGroups[i]);
				String groupName = groupData.get("_id").getAsString();
				String groupMembers = groupData.get("groupMembers").getAsString();
				grouplist += "<span><a href='chat.jsp?chatgroupID=" + groupName + "'>" + groupName + "</a></span><br />";
				
			}
			catch (NoDocumentException e) {
				System.out.println("Nothing found for " + joinedGroups[i]);
			}
		} 
		
		USER.addProperty("listofgroups",grouplist);
	}					
    %>
   <%//variables for java script 
     					// If Owner
						String deletegrouplist ="";
     					if(groupOwner.equals(USER.get("email").getAsString())) {
     						deletegrouplist = "<span id='mapnav2'><a href='DeleteGroup?chatgroupID="+chatGroupID+"'>DELETE GROUP</a>";
						} 
						//out.println("<u><b>Members</b></u>");
						String deletememberslist = " ";
						for(int i = 0; i < allGroupMembers.length; i++) {
							
							if(groupOwner.equals(USER.get("email").getAsString()) &&
									!allGroupMembers[i].equals(USER.get("email").getAsString())) {
								deletememberslist +=  "<span><a href='DeleteMember?group="+chatGroupID+"&member="+allGroupMembers[i]+"'>"+allGroupMembers[i]+"</a></span><br />";
							}
							
							
						}
						deletememberslist += " ";
						System.out.println(deletememberslist);
						%>
<!--  /FIND COOKIE INFO -->
	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>R3GISTRA | <%= USER.get("email").getAsString() %></title>
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script type="text/javascript" src="jquery/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="jquery/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main-home.js"></script>
    <script src="js/chat.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script> //jsp specific function calls 
			
		var userEmail = <%= USER.get("email") %>
	
		$(document).ready(function() {
			//turn management properties off
			if( <%= groupOwner.equals(USER.get("email").getAsString()) %> ){
				
			} else {
				$("#management-properties").hide();
			}
			
		});
		function logout(){
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>	
			$.ajax({
				type : "post",
				url : "Logout",
				data : datastr,
				success : function(msg) {
					if(/^\s*TRUE\s*$/.test(msg)) {
						window.location = "index.jsp";
					}
					else if(/^\s*FALSE\s*$/.test(msg)) {
						$('#alertlogout').html("");
					}
					
				}
			});
		}
		function redirect(){
			window.location = "index.jsp";
		}
		function resetUser() {
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>
			$('#alertreset').html("<h8>resetting...<h8>");
			$.ajax({
				type : "post",
				url : "Reset",
				data : datastr,
				success : function(msg) {
					if (/^\s*success\s*$/.test(msg))
					$('#alertreset').html("<h8>" + msg + "<h8>");
					setTimeout(redirect, 2000);
				}
			});
		}
		function deleteUser() {
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>
			$('#alertreset').html("<h8>deletting...<h8>");
			$.ajax({
				type : "post",
				url : "Delete",
				data : datastr,
				success : function(msg) {
					if(/^\s*TRUE\s*$/.test(msg)) {
						window.location = "index.jsp";
					}
					else if(/^\s*FALSE\s*$/.test(msg)) {
						$('#alertlogout').html("<h8>You are not logged in.<h8>");
					}
				}
			});
		}
		var groupOn = false;
		var notificationOn = false;
		var createGroupOn = false;
		var managementOn = false;
		var addMemberOn = false;
		var deleteMemberOn = false;
		var deleteGroupOn = false;
		function menu(){/*sets up div box containing menu items */}
		
		function checkNotifications(){
			if ( notificationOn ){
				$('#mapnav-1').html('');
				$('#notificationResponse').html('');	
				notificationOn = !notificationOn;
			}else{	
				$('#mapnav-1').html(">");
				$('#notificationResponse').html('');
				notificationOn = !notificationOn;
			}
		}
		function myGroups(){
			if ( groupOn ){
				$('#mapnav-2').html('');
				$('#groupResponse').html('');	
				groupOn = !groupOn;
			}else{	
				$('#mapnav-2').html(">");	
				$('#groupResponse').html(''+ <%= USER.get("listofgroups") %> +'');
				groupOn = !groupOn;
			}
		}
		function management(){
			if ( managementOn ){
				$('#mapnav-4').html('');
				$('#managementResponse').html("");
				managementOn = !managementOn;
			}else{	
				if (addMemberOn){addMemberOn = !addMemberOn;}
				if (deleteMemberOn){deleteMemberOn = !deleteMemberOn;}
				if (deleteGroupOn){deleteGroupOn = !deleteGroupOn;}
				
					$('#mapnav-4').html(">");	
				
			   		 $('#managementResponse').html(""+	
					"<span class='mapnav2'><span id='mapnav-5' class='mapnav2'></span><a class='mapnavlinks' href='javascript:addMember()'>ADD MEMBER</a><br /></span>"+
		    		"<div id='addMemberResponse'></div>");
			    
			    	$('#managementResponse').append(""+
		    		"<span class='mapnav2'><span id='mapnav-6' class='mapnav2'></span><a class='mapnavlinks' href='javascript:deleteMember()'>DELETE MEMBER</a><br /></span>"+
		    		"<div id='deleteMemberResponse'>"+
		    		"</div>");
			 
			    	$('#managementResponse').append(""+
		    		"<span class='mapnav2'><span id='mapnav-7' class='mapnav2'></span><a class='mapnavlinks' href='DeleteGroup?chatgroupID=<%= chatGroupID %>' >DELETE GROUP</a><br /></span>"+
		    		"<div id='deleteGroupResponse'>"+	
		    		"</div>");
			    managementOn = !managementOn;
			}
		}
		function deleteMember(){
			if ( deleteMemberOn ){
				$('#mapnav-6').html('');
				$('#deleteMemberResponse').html('');	
				deleteMemberOn = !deleteMemberOn;
			}else{	
				$('#mapnav-6').html(">");	
				$('#deleteMemberResponse').html("<%= deletememberslist %>");
				deleteMemberOn = !deleteMemberOn;
			}		
		}	
		function addMember(){
			if ( addMemberOn ){
				$('#mapnav-5').html('');
				$('#addMemberResponse').hide();	
				addMemberOn = !addMemberOn;
			}else{	
				$('#mapnav-5').html(">");	
				$('#addMemberResponse').html("<div id='regiterform' class='input-group'>"+
						 "<form id='invite' action='javascript:invite()' method='post'>"+
						 "<input id='emailinvite' style='margin-top:-2px;' type='text' name='email' size='30' placeholder='Email Address'/>"+	
						 "<input type='submit' name='submit' style='display: none'/>"+
						 "</form></div>");
				addMemberOn = !addMemberOn;
			}	
		}
		function createGroup(){
			if ( createGroupOn ){
				$('#mapnav-3').html('');
				$('#createGroupResponse').hide();
				createGroupOn = !createGroupOn;
			}else{	
				$('#mapnav-3').html(">");	
				$('#createGroupResponse').show();
				createGroupOn = !createGroupOn;
			}
		}
		var chatURL = ""; 
	    function CreateGroupForm(){
	    	var groupName = $('#groupName').val();
			var chatURL = "groupName=" + $('#groupName').val();
			$.ajax({
				type : "post",
				url : "CreateGroup",
				data : chatURL,
				success : function(msg) {
					
					if(/^\s*CREATED\s*$/.test(msg)) {
						
						// Show confirmation
						$('#groupName').val('');
						$('#groupName').attr("placeholder",msg);
						
						chatURL = "chat.jsp?chatgroupID=" + groupName;
						
						// redirect to chat page
						
						window.location = chatURL;
						
					}
					else {
						// Show error
						$('#groupName').val('');
						$('#groupName').attr("placeholder",msg);
					}
				}
			});	
		}
	    function validateEmail(email){
	    	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	    	var valid = emailReg.test(email);

	    	if(!valid) {
	            return false;
	        } else {
	        	return true;
	        }
	    }
		function invite(){
			var emailinvite = $('#emailinvite').val();
			var datastr = "email=" + emailinvite + "&group=" + <%= USER.get("group") %> ;
			var tmp = validateEmail(emailinvite);
			$('#emailinvite').val('');
			$('#emailinvite').attr("placeholder","loading...");
			if (!tmp){ 
		    $('#emailinvite').val('');
			//relay email and group to add to 
			$('#emailinvite').attr("placeholder","Invalid email address");
		    } else {$.ajax({
				type : "post",
				url : "Invite",
				data : datastr,
				success : function(msg) {
				//relay email and group to add to 
				$('#emailinvite').val('');
				$('#emailinvite').attr("placeholder",msg);	
				}
			});		
		    }
		}
	</script>
</head>

<body>
	
	<div id="R3G-navbar" class="navbar R3G-nav">
		<div class="container">
			<div class="navbar-header">
			<a class="navbar-brand R3G-link" href="index.jsp">R3GISTRA |  <%= USER.get("email").getAsString() %></a>
			</div>  
			   <div class="">
  
        		<ul class="nav navbar-nav pull-right">
         		 <li><a href="javascript:logout()">Log Out</a></li>
         		 <li class=""><div id="alertlogout" style="margin-top:15px;"></div></li>
         		 <li class="divider-vertical"></li>
         		 
         		 <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" onclick="menu()">Profile<strong class="caret"></strong></a>
            	<div id="signup" class="dropdown-menu" style="padding: 10px; padding-bottom: 10px;">
       				<div id="profile">
       				<a href="javascript:resetUser()">Reset</a>
         		    <div id="alertreset"></div>
         		    <a href="javascript:deleteUser()">Delete</a>
         		    <div id="alertdelet"></div>
       				</div>		
            	</div>
         		</li>
        		</ul>
			</div>
		</div>
	</div>
	<div id="R3G-body" class="container-fluid" >	 
	   	 <div class="container">
	   	 	<table id="R3G-body-table" class="">
	   	 	<tr class="container">
	   	 	<td>
	   	 	<div id="R3G-mapnav">
	   		<div class="wrapper">
	   		<!--  functions for this nav at the bottom -->
	   		<span class="mapnav1"> >MENU</span>
	   			<div class="first">
	   				<span class="mapnav2"><span id="mapnav-1" class="mapnav2"></span><a class="mapnavlinks" href="javascript:checkNotifications()">NOTIFICATIONS</a><br /></span>
	   				<div id="notificationResponse"></div>
	   				<span class="mapnav2"><span id="mapnav-2" class="mapnav2"></span><a class="mapnavlinks" href="javascript:myGroups()">MY GROUPS</a><br /></span>
	   				<div id="groupResponse"></div>
	   				<span class="mapnav2"><span id="mapnav-3" class="mapnav2"></span><a class="mapnavlinks" href="javascript:createGroup()" >CREATE GROUP</a><br /></span>
	   				<div id="createGroupResponse">
	   					<form id='createGroupForm' action='javascript:CreateGroupForm()' method='post'>
						 	<input id='groupName' style='margin-top:-2px;' type='text' name='groupName' size='30' placeholder='Group Name'/>	
						 	<input id='createGroupSubmit' type='submit' style='display: none'/>
					 	</form>
	   				</div>
	   			</div>
	   			<div id="management-properties" class="first">
	   				<span class="mapnav2"><span id="mapnav-4" class="mapnav2"></span><a class="mapnavlinks" href="javascript:management()">MANAGE - <%= chatGroupID.toUpperCase() %></a><br /></span>
	   					<div id="managementResponse"></div>
	   			</div>
	   		</div>
	   	 	</div>
	   	 	</td>
	   	 	<td id="R3G-chatbody" class="" style="width:100%;position:relative;">
		  		<div id="chatbody" style="width:100%;height:100%;">
					<div id="groupIDHolder" style="display:none;">
					<%= chatGroupID %>
					</div>
					<div class="" style="position:absolute;top:0;right:0;"><h3 class="chatName" ><%= chatGroupID %></h3></div>
					<div id="chat" class="" style="width:100%;">
					</div>	
				   	<div id="formArea" class="" style="position:absolute;bottom:5px;right:0;">	
				   	<form id="chatform" method="post">
						<!-- Name: <input id="name" type="text" name="name" />  -->
						<input id="message" type="text" name="message" placeholder="message" />
					    <!--  Hidden field to past groupID -->
					    <input id="groupID" type="hidden" name="groupID" value='<%= chatGroupID %>'>
					    
						<input id="button" type="hidden" value="send" name="button" />
					</form>
					</div>
				</div>
			
			</td>
			</tr>
	   	 	</table></div>
	</div>
	<div id="R3G-footer" class="container-fluid">
		<div class="container">
			<div class="row">
				<span class="R3G-container R3G-footer">&#169;2013&nbsp;|&nbsp;<a href="http://www.ryanleong.me" class=".rj-credits-dev-link" target="_blank">ryanleong</a>&nbsp;|&nbsp; 
				<a href="http://www.wilkosz.com.au" class=".rj-credits-dev-link" target="_blank">joshuawilkosz</a></span> 
			</div>
		</div>
	</div>
	<div id="dialog" title="Alert"></div>
</body>
<script>
$(function() {
	  // Setup drop down menu
	  $('.dropdown-toggle').dropdown();
	 
	  // Fix input element click problem
	  $('.dropdown input, .dropdown label').click(function(e) {
		  e.stopPropagation();
	  });
	});
</script>
</html>

 
