<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="com.google.gson.JsonObject" import="java.util.io.*" 
import="javax.servlet.RequestDispatcher"
import="javax.servlet.ServletException"
import="javax.servlet.annotation.WebInitParam"
import="javax.servlet.annotation.WebServlet"
import="javax.servlet.http.HttpServlet"
import="javax.servlet.http.HttpServletRequest"
import="javax.servlet.http.HttpServletResponse" 
import="org.lightcouch.CouchDbClient"
import="org.lightcouch.NoDocumentException"
import="org.lightcouch.CouchDbProperties"
%>
 
<!--  FIND COOKIE INFO -->
<%  CouchDbClient dbClient = new CouchDbClient();
	String access = "R3GISTRA_access";  		//changed cookie name for deliminator
	JsonObject USER = new JsonObject();     //object to use for response
	USER.addProperty("valid", false);
	USER.addProperty("email","");
	javax.servlet.http.Cookie[] cookies = request.getCookies(); 
	 
	JsonObject userData = null;
	
	for (int i=0;i<(cookies==null?0:cookies.length);i++){ //checks for null cookies (if zero) 
		javax.servlet.http.Cookie cookie = cookies[i];   
			
		if(access.equals(cookie.getName())) {
			
			try {
				String token = cookie.getValue().trim();
				String [] tokens = token.split("\\|");
				
				//build Json data - maybe used for sessions data
				userData = dbClient.find(JsonObject.class, tokens[0]);
						
				//check if cookie is valid	
				if ((userData.get("accesstoken").getAsString()).equals(tokens[1])){
				    USER.addProperty("valid",true);
				    USER.addProperty("email",tokens[0]);
				    USER.addProperty("accesstoken", tokens[1]);
				} else {
					USER.addProperty("valid",false);
				}
			} catch (NoDocumentException e) {
				// TODO: handle exception
				System.err.println("error user no longer in couchdb");
			} 
		} 
	} 
	if (USER.get("valid").getAsBoolean()==false) {
		RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
		RD.forward(request, response);
		return;
	}
	//get joinedGroup Session
	//String JoinedGroups = userData.get("joinedGroups").getAsString();
	//String[] joinedGroups = JoinedGroups.split(",");
	%>	
	<%          String grouplist = null; 
				CouchDbClient databaseClient = new CouchDbClient();
				CouchDbProperties properties = new CouchDbProperties()
				.setDbName("groups")
				.setCreateDbIfNotExist(true)
				.setProtocol("http")
				.setHost("127.0.0.1").setPort(5984)
				.setMaxConnections(100)
				.setConnectionTimeout(0);

				databaseClient = new CouchDbClient(properties);
				
				// Get all groups user is in
				String userJoinedGroups = userData.get("joinedGroups").getAsString();
				String[] joinedGroups = userJoinedGroups.split(",");
				if(userJoinedGroups.equals("")) {
					grouplist = "No Available Groups";
				}
				else {
					grouplist = "";
					for(int i = 0; i < joinedGroups.length; i++) {		
						
						try {
							JsonObject groupData = databaseClient.find(JsonObject.class, joinedGroups[i]);
							String groupName = groupData.get("_id").getAsString();
							String groupMembers = groupData.get("groupMembers").getAsString();
							grouplist += "<span><a href='chat.jsp?chatgroupID=" + groupName + "'>" + groupName + "</a></span><br />";
							
						}
						catch (NoDocumentException e) {
							System.out.println("Nothing found for " + joinedGroups[i]);
						}
					}
					USER.addProperty("listofgroups",grouplist);
				}
				System.out.println(grouplist);
									
									
			    %>
	
<!--  /FIND COOKIE INFO -->

<!--  Get Group Data -->


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>R3GISTRA | <%= USER.get("email").getAsString() %></title>
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script type="text/javascript" src="jquery/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="jquery/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main-home.js"></script>
  
    <script src="js/navigation.js"></script>
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script> //jsp specific function calls 
		$(document).ready(function() {});
		
	    function logout(){
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>	
			$.ajax({
				type : "post",
				url : "Logout",
				data : datastr,
				success : function(msg) {
					if(/^\s*TRUE\s*$/.test(msg)) {
						window.location = "index.jsp";
					}
					else if(/^\s*FALSE\s*$/.test(msg)) {
						$('#alertlogout').html("");
					}
					
				}
			});
		}
		
		function redirect(){
			window.location = "index.jsp";
		}
		function resetUser() {
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>
			$('#alertreset').html("<h8>resetting...<h8>");
			$.ajax({
				type : "post",
				url : "Reset",
				data : datastr,
				success : function(msg) {
					if (/^\s*success\s*$/.test(msg))
					$('#alertreset').html("<h8>" + msg + "<h8>");
					setTimeout(redirect, 2000);
				}
			});
		}
		function deleteUser() {
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>
			$('#alertreset').html("<h8>deletting...<h8>");
			
			$.ajax({
				type : "post",
				url : "Delete",
				data : datastr,
				success : function(msg) {
					if(/^\s*TRUE\s*$/.test(msg)) {
						window.location = "index.jsp";
					}
					else if(/^\s*FALSE\s*$/.test(msg)) {
						$('#alertlogout').html("<h8>You are not logged in.<h8>");
					}
				}
			});
		}
		
		function createGrouptmp() {
			$('#createError').html("");
			$( "#create" ).dialog("open");
		}
		var groupOn = false;
		var notificationOn = false;
		var createGroupOn = false;
		function menu(){/*sets up div box containing menu items */}
		
		function checkNotifications(){
			if ( notificationOn ){
				$('#mapnav-1').html('');
				$('#notificationResponse').html('');	
				notificationOn = !notificationOn;
			}else{	
				$('#mapnav-1').html(">");
				$('#notificationResponse').html('');
				notificationOn = !notificationOn;
			}
		}
		function myGroups(){
			if ( groupOn ){
				$('#mapnav-2').html('');
				$('#groupResponse').html('');	
				groupOn = !groupOn;
			}else{	
				$('#mapnav-2').html(">");	
				$('#groupResponse').html(''+ <%= USER.get("listofgroups") %> +'');
				groupOn = !groupOn;
			}
		}
		function createGroup(){
			if ( createGroupOn ){
				$('#mapnav-3').html('');
				$('#createGroupResponse').hide();
				createGroupOn = !createGroupOn;
			}else{	
				$('#mapnav-3').html(">");	
				$('#createGroupResponse').show();
				createGroupOn = !createGroupOn;
			}
		}
			var chatURL = ""; 
		    function CreateGroupForm(){
		    var groupName = $('#groupName').val();
			var chatURL = "groupName=" + $('#groupName').val();
			
			$.ajax({
				type : "post",
				url : "CreateGroup",
				data : chatURL,
				success : function(msg) {
					var chatInfo = $('#groupName').val();
					$('#groupName').val("");
					if(/^\s*CREATED\s*$/.test(msg)) {
						
						// Show confirmation
						$('#groupName').val('');
						$('#groupName').attr("placeholder",msg);

						chatURL = "chat.jsp?chatgroupID=" + chatInfo;

						// redirect to chat page
						setTimeout(function(){/*   */},1000);
						window.location = chatURL;
						
					}
					else {
						// Show error
						$('#groupName').val('');
						$('#groupName').attr("placeholder",msg);
					}
				}
			});	
		}
	</script>

</head>
<body>


	
	<div id="R3G-navbar" class="navbar R3G-nav">
		<div class="container">
			<div class="navbar-header">
			<a class="navbar-brand R3G-link" href="index.jsp">R3GISTRA |  <%= USER.get("email").getAsString() %></a>
			</div>  
			   <div class="">
        		<ul class="nav navbar-nav pull-right">
         		 <li><a href="javascript:logout()">Log Out</a></li>
         		 <li class=""><div id="alertlogout" style="margin-top:15px;"></div></li>
         		 <li class="divider-vertical"></li>
         		 
         		 <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" onclick="menu()">Profile<strong class="caret"></strong></a>
            	<div id="signup" class="dropdown-menu" style="padding: 10px; padding-bottom: 10px;">
       				<div id="profile">
       				<a href="javascript:resetUser()">Reset</a>
         		    <div id="alertreset"></div>
         		    <a href="javascript:deleteUser()">Delete</a>
         		    <div id="alertdelet"></div>
       				</div>		
            	</div>
         		</li>
        		</ul>
			</div>
		</div>
	</div>
	<div id="R3G-body" class="container-fluid" style="overflow-x:hidden;">	 
	   	 <div class="container">
	   	 	<table id="R3G-body-table" class="">
	   	 	<tr class="container">
	   	 	<td>
	   	 	<div id="R3G-mapnav">
	   		<div class="wrapper">
	   		<!--  functions for this nav at the bottom -->
	   		<span class="mapnav1"> >MENU</span>
	   			<div class="first">
	   				<span class="mapnav2"><span id="mapnav-1" class="mapnav2"></span><a class="mapnavlinks" href="javascript:checkNotifications()">NOTIFICATIONS</a><br /></span>
	   				<div id="notificationResponse"></div>
	   				<span class="mapnav2"><span id="mapnav-2" class="mapnav2"></span><a class="mapnavlinks" href="javascript:myGroups()">MY GROUPS</a><br /></span>
	   				<div id="groupResponse"></div>
	   				<span class="mapnav2"><span id="mapnav-3" class="mapnav2"></span><a class="mapnavlinks" href="javascript:createGroup()" >CREATE GROUP</a><br /></span>
	   				<div id="createGroupResponse">
	   					<form id='createGroupForm' action='javascript:CreateGroupForm()' method='post'>
						 	<input id='groupName' style='margin-top:-2px;' type='text' name='groupName' size='30' placeholder='Group Name'/>	
						 	<input id='createGroupSubmit' type='submit' style='display: none'/>
					 	</form>
	   				</div>
	   			</div>
	   		</div>
	   	 	</div>
	   	 	</td><td class="" style="width:100%;position:relative;">
		  		<div id="chatbody">
					
				</div>
			
			</td></tr>
	   	 	</table></div>
	</div>
	<div id="R3G-footer" class="container-fluid">
		<div class="container">
			<div class="row">
				<span class="R3G-container R3G-footer">&#169;2013&nbsp;|&nbsp;<a href="http://www.ryanleong.me" class=".rj-credits-dev-link" target="_blank">ryanleong</a>&nbsp;|&nbsp; 
				<a href="http://www.wilkosz.com.au" class=".rj-credits-dev-link" target="_blank">joshuawilkosz</a></span> 
			</div>
		</div>
	</div>
	<div id="dialog" title="Alert"></div>
</body>
<script>
$(function() {
	  // Setup drop down menu
	  $('.dropdown-toggle').dropdown();
	 
	  // Fix input element click problem
	  $('.dropdown input, .dropdown label').click(function(e) {
		  e.stopPropagation();
	  });
	});
</script>
</html>

 

