package functions;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RandomStringGenerator {
	
	public RandomStringGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public String generateString(String hashValue) {
		
		try {
			// Specify to use MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(hashValue.getBytes());
			
			// Get byte stream of hash value
			byte[] byteData = md.digest();
			
	        // Convert the byte to hex format
	        StringBuffer sb = new StringBuffer();
	        
	        for (int i = 0; i < byteData.length; i++) {
	        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
	        return sb.toString();
			
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Failed to generated random string. Processes aborted.");
			e.printStackTrace();
			return null;
		}
	}
}
