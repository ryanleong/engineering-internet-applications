package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonObject;

import functions.EmailHandler;
import functions.RandomStringGenerator;

/**
 * Servlet implementation class Invite
 */
@WebServlet(
		urlPatterns = { "/Invite" }, 
		initParams = { 
				@WebInitParam(name = "Invite", value = "")
		})
public class Invite extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CouchDbClient databaseClient = new CouchDbClient();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Invite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		String group = request.getParameter("group");
		String emailInvite = request.getParameter("email");
		String cookieData = null;
		
		// Get cookie data
		Cookie[] cookies = request.getCookies();
		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i].getName().equals("R3GISTRA_access")) {
				cookieData = cookies[i].getValue();
				break;
			}
		}
		// Split cookie data
		String[] data = cookieData.split("\\|");
		String email = data[0];
		String accessToken = data[1];
		
		
		
		// Set connection to "users" database
		CouchDbProperties properties = new CouchDbProperties()
		.setDbName("users")
		.setCreateDbIfNotExist(true)
		.setProtocol("http")
		.setHost("127.0.0.1").setPort(5984)
		.setMaxConnections(100)
		.setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		JsonObject user = null;
		
		// Check if user is logged in
		try {
			user = databaseClient.find(JsonObject.class, email);
			
			if(!user.get("accesstoken").getAsString().equals(accessToken)) {
				out.println("You are not logged in.");
				return;
			}
		} catch (NoDocumentException e) {
			out.println("Invalid Account.");
			return;
		}
		if (email.equalsIgnoreCase(emailInvite)){
			out.println("cannot add yourself!");
			return;
		} else { 
			//Check that invited user is confirmed
			try {
				user = databaseClient.find(JsonObject.class, emailInvite);
			
				if(!user.get("confirmed").getAsBoolean()) {
					out.println("Invalid Account.");
					return;
				} else if ((user.get("joinedGroups").getAsString()).contains(group)){
					out.println("Already a member");
					return;
				}
			} catch (NoDocumentException e) {
				out.println("Invalid Account.");
				return;
			}	
			// Set connection to "groups" database
			properties = new CouchDbProperties()
			.setDbName("groups")
			.setCreateDbIfNotExist(true)
			.setProtocol("http")
			.setHost("127.0.0.1").setPort(5984)
			.setMaxConnections(100)
			.setConnectionTimeout(0);
			databaseClient = new CouchDbClient(properties);
			
			if(!databaseClient.contains(group)) {
				out.println("No such group");
				return;
			} else {
				JsonObject groupCouchdb = databaseClient.find(JsonObject.class, group);
				String token = groupCouchdb.get("groupAccessToken").getAsString();
			
				String baseURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(1),request.getContextPath());
				System.out.println(baseURL);
				String registrationMessage = email+" has invited to join "+ group + ". To confirm click:" + 
						baseURL + 
						"/ConfirmInvite?email=" + emailInvite +
						"&group="+group.replace(" ","%20")+
						"&token="+token;
				EmailHandler handler = new EmailHandler();
				handler.sendEmail(email, "Chat Group Registration", registrationMessage);
				// Return confirmation
				out.println("Information sent to user");
			}
		}
	}
	
}
