package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ProcessBuilder.Redirect;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonObject;


/**
 * Servlet implementation class DeleteMember
 */
@WebServlet(
		urlPatterns = { "/DeleteMember" }, 
		initParams = { 
				@WebInitParam(name = "DeleteMember", value = "")
		})
public class DeleteMember extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private CouchDbClient databaseClient;
	private JsonObject groupDetails = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteMember() {
        super();
        
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("users")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
    
		databaseClient = new CouchDbClient(properties);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String group = request.getParameter("group");
		String member = request.getParameter("member");
		

		// Check all rights
		if (!checkRights(request.getCookies(), group)) {
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
		
		System.out.println("Current group members: " + groupDetails.get("groupMembers").getAsString());
		
		// Delete Member
		String[] groupMembers = groupDetails.get("groupMembers").getAsString().split(",");
		
		String updatedGroupMembers = "";
		boolean inGroup = false;
		
		for (int i = 0; i < groupMembers.length; i++) {
			
			if (!groupMembers[i].equals(member)) {
				updatedGroupMembers = updatedGroupMembers + groupMembers[i]+ ","; 
			}
			else {
				inGroup = true;
			}
		}
		
		// If user not in group, redirect
		if (!inGroup) {
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
		
		System.out.println("Update group member list: " + updatedGroupMembers);
		
		// Remove user from group
		groupDetails.addProperty("groupMembers", updatedGroupMembers);
		databaseClient.update(groupDetails);
		
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("users")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		JsonObject user = databaseClient.find(JsonObject.class, member);
		
		System.out.println(user.get("joinedGroups").getAsString());
		
		String updateUserGroups = "";
		String[] userGroupData = user.get("joinedGroups").getAsString().split(",");
		
		for (String userGroup : userGroupData) {
			if (!userGroup.equals(group)) {
				updateUserGroups += userGroup + ",";
			}
		}
		
		System.out.println("UpdatedUserGroups: " + updateUserGroups);
		
		// Update user's groups
		user.addProperty("joinedGroups", updateUserGroups);
		databaseClient.update(user);
		
		System.out.println(member + " has been removed from group " + group);
		
		// Redirect back to group page
		RequestDispatcher RD = request.getRequestDispatcher("chat.jsp?chatgroupID=" + group);
		RD.forward(request, response);
	}

	private boolean checkRights(Cookie[] cookie_jar, String group) {
		String cookieString = "";
		boolean isLoggedIn = false;
		
		// Check to see if any cookies exists
		if (cookie_jar == null) {
			// Redirect
			return false;
		}
		
		for (int i =0; i< cookie_jar.length; i++) {
			Cookie aCookie = cookie_jar[i];
			
			if (aCookie.getName().equals("R3GISTRA_access")) {	
				cookieString = aCookie.getValue();
				isLoggedIn = true;
				break;
			}
		}
		
		if (!isLoggedIn) {
			// Redirect
			return false;
		}


		String[] cookieData = cookieString.split("\\|");
		
		// Check if user is logged in
		try {
			JsonObject user = databaseClient.find(JsonObject.class, cookieData[0]);
			
			if (!user.get("accesstoken").getAsString().equals(cookieData[1])) {
				// Redirect
				return false;
			}
		}
		catch(NoDocumentException e) {
			// No such user
			// Redirect
			return false;
		}
		
		System.out.println("Logged In.");
		
		// Check if user is owner of group
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
  
		databaseClient = new CouchDbClient(properties);
		
		
		try {
			groupDetails = databaseClient.find(JsonObject.class, group);
			
			if (!groupDetails.get("groupOwner").getAsString().equals(cookieData[0])) {
				// Redirect
				return false;
			}
		}
		catch (NoDocumentException e) {
			// No such group
			// redirect
			return false;
		}
		
		System.out.println("Is Owner.");
		
		// Check if member to be deleted is in group
		String[] groupMembers = groupDetails.get("groupMembers").getAsString().split(",");
		
		
		return true;
	}
}
