package core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;

import com.google.gson.JsonObject;

import functions.EmailHandler;
import functions.RandomStringGenerator;

/**
 * Servlet implementation class ConfirmInvite
 */
@WebServlet(
		urlPatterns = { "/ConfirmInvite" }, 
		initParams = { 
				@WebInitParam(name = "ConfirmInvite", value = "")
		})
public class ConfirmInvite extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static CouchDbClient dbGroup = new CouchDbClient();
	private static CouchDbClient dbClient = new CouchDbClient();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmInvite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			PrintWriter out = response.getWriter();
			
			// Get email and token from confirmation link
			String email = request.getParameter("email").toLowerCase();
			String token = request.getParameter("token");
			String group = request.getParameter("group");
			System.out.println(email+token);
			
			//check to see if token matches group token 
			CouchDbProperties Groupproperties = new CouchDbProperties()
			.setDbName("groups")
			.setCreateDbIfNotExist(true)
			.setProtocol("http")
			.setHost("127.0.0.1").setPort(5984)
			.setMaxConnections(100)
			.setConnectionTimeout(0);
			dbGroup = new CouchDbClient(Groupproperties);
			
			//check to see if user is confirmed 
			CouchDbProperties Userproperties = new CouchDbProperties()
			.setDbName("users")
			.setCreateDbIfNotExist(true)
			.setProtocol("http")
			.setHost("127.0.0.1").setPort(5984)
			.setMaxConnections(100)
			.setConnectionTimeout(0);
			dbClient = new CouchDbClient(Userproperties);
			
			// make sure both still exish
			if (!dbGroup.contains(group) | !dbClient.contains(email) ){
				out.println("group no longer exists or your account has been deleted");
				return;
			}
			JsonObject USER = dbClient.find(JsonObject.class, email);
			JsonObject GROUP = dbGroup.find(JsonObject.class, group);
			if (!GROUP.get("groupAccessToken").getAsString().equals(token)){
				out.println("access token out of date");	
				return;
			} else if (!USER.get("confirmed").getAsBoolean()){
				out.println("user not confirmed");	
				return;
			} else {
				//update both user and group with new details	
				String groupMembers = GROUP.get("groupMembers").getAsString();
				groupMembers += email+",";
				GROUP.addProperty("groupMembers",groupMembers);
				dbGroup.update(GROUP);
				//update user
				String joinedGroups = USER.get("joinedGroups").getAsString();
				joinedGroups += group+",";
				USER.addProperty("joinedGroups",joinedGroups);
				dbClient.update(USER);
				out.println("you have been added to "+group+"'s group");
				return;
			}	
	}		
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
		}

	}
