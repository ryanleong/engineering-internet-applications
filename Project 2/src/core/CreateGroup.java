package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonObject;

import functions.RandomStringGenerator;

/**
 * Servlet implementation class CreateGroup
 */
@WebServlet(urlPatterns = { "/CreateGroup" }, initParams = { @WebInitParam(name = "CreateGroup", value = "") })
public class CreateGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// coudb properties set to class path
	private CouchDbClient databaseClient = new CouchDbClient();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateGroup() {
		super();
		
		CouchDbProperties properties = new CouchDbProperties()
				.setDbName("users")
				.setCreateDbIfNotExist(true)
				.setProtocol("http")
				.setHost("127.0.0.1").setPort(5984)
				.setMaxConnections(100)
				.setConnectionTimeout(0);

		databaseClient = new CouchDbClient(properties);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		String groupName = request.getParameter("groupName");
		String cookieData = null;
		
		// Get cookie data
		Cookie[] cookies = request.getCookies();
		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i].getName().equals("R3GISTRA_access")) {
				cookieData = cookies[i].getValue();
				break;
			}
		}
		
		// Split cookie data
		String[] data = cookieData.split("\\|");
		String email = data[0];
		String accessToken = data[1];
		
		// Set connection to "users" database
		CouchDbProperties properties = new CouchDbProperties()
		.setDbName("users")
		.setCreateDbIfNotExist(true)
		.setProtocol("http")
		.setHost("127.0.0.1").setPort(5984)
		.setMaxConnections(100)
		.setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		JsonObject user = null;
		
		// Check if user is logged in
		try {
			user = databaseClient.find(JsonObject.class, email);
			
			if(!user.get("accesstoken").getAsString().equals(accessToken)) {
				out.println("You are not logged in.");
				return;
			}
		} catch (NoDocumentException e) {
			out.println("Invalid Account.");
			return;
		}
		
		//check if group already exists 

		// Update user's joined groups
		String joinedGroups = user.get("joinedGroups").getAsString();
			if(!user.get("joinedGroups").getAsString().contains(groupName)){
				user.addProperty("joinedGroups", (joinedGroups + groupName + ","));
				databaseClient.update(user);
			} else {
				out.println("user is already a member of that group");
				return;
			}
		// Set connection to "groups" database
		properties = new CouchDbProperties()
		.setDbName("groups")
		.setCreateDbIfNotExist(true)
		.setProtocol("http")
		.setHost("127.0.0.1").setPort(5984)
		.setMaxConnections(100)
		.setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);

		if(databaseClient.contains(groupName)) {
			System.out.println("name already exists");
			out.println("Group name in use.");
			return;
		}
		else {
			// Create a user JsonObject
			JsonObject newGroup = new JsonObject();
			newGroup.addProperty("_id", groupName);
			newGroup.addProperty("groupAccessToken", new RandomStringGenerator().generateString((new Date()).toString()));
			newGroup.addProperty("groupOwner", user.get("_id").getAsString());
			newGroup.addProperty("groupMembers", user.get("_id").getAsString() + ",");
			// TODO add creation timestamp

			// Insert new user to DB
			databaseClient.save(newGroup);

			
			// Return confirmation
			out.println("CREATED");
		}
		
	}
	

}
