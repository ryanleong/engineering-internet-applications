package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.util.StringParser;
import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class Delete
 */
@WebServlet(
		urlPatterns = { "/Delete" }, 
		initParams = { 
				@WebInitParam(name = "Delete", value = "")
		})
public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private JsonObject userData = null;
	private ArrayList<String> usersToUpdate = new ArrayList<String>();
	private String[] userCreatedGroups; 
	
	// Connection to DB
	private static CouchDbClient dbClient = new CouchDbClient();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		// Check all rights
		if (!checkRights(request.getCookies())) {
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
		
		// Get Token
		String token = request.getParameter("token");
		String email = request.getParameter("email").toLowerCase();
		
		// Get client data
		try {
			userData = dbClient.find(JsonObject.class, email);
		} catch (Exception e) {
			// No such user
			System.out.println("No such user in database.");
			// Redirect
			out.println("FALSE");
			return;
		}
		
		String tokenString = userData.get("accesstoken").getAsString();
		
		// Check token to see if logged in
		if (tokenString.equals(token)) {
			try {
				// Remove groups belonging to user
				removeUserGroups();
				
				
				CouchDbProperties properties = new CouchDbProperties()
				  .setDbName("users")
				  .setCreateDbIfNotExist(true)
				  .setProtocol("http")
				  .setHost("127.0.0.1")
				  .setPort(5984)
				  .setMaxConnections(100)
				  .setConnectionTimeout(0);
				dbClient = new CouchDbClient(properties);
				
				// Remove group data from other users in the above deleted groups
				for (String deletedGroup : userCreatedGroups) {
					
					removeGroupData(deletedGroup);
				}
				
				properties = new CouchDbProperties()
				  .setDbName("users")
				  .setCreateDbIfNotExist(true)
				  .setProtocol("http")
				  .setHost("127.0.0.1")
				  .setPort(5984)
				  .setMaxConnections(100)
				  .setConnectionTimeout(0);
				dbClient = new CouchDbClient(properties);
				
				System.out.println("All data deleted. Removing account.");
				
				// Delete account
				userData = dbClient.find(JsonObject.class, email);
				dbClient.remove(userData);
				out.println("TRUE");
			} catch (Exception e) {
				out.println("ERROR");		
			}
		}
		else {
			out.println("FALSE");
		}
	}
	

	private void removeGroupData(String deletedGroup) {
		
		
		for (String user : usersToUpdate) {
			try {
				JsonObject otherUserData = dbClient.find(JsonObject.class, user);
				
				String[] userJoinedGroups = otherUserData.get("joinedGroups").getAsString().split(",");
				String updatedUserJoinedGroups = "";
				
				for (String groupName : userJoinedGroups) {
					
					if (!groupName.equals(deletedGroup)) {
						updatedUserJoinedGroups += groupName + ",";
					}
				}

				System.out.println("User, " + user + ", updated groups: " + updatedUserJoinedGroups);
				
				// Update user
				otherUserData.addProperty("joinedGroups", updatedUserJoinedGroups);
				dbClient.update(otherUserData);
				
			} catch (NoDocumentException e) {
				System.out.println("No such user");
				continue;
			}
		}
	}
	
	private void removeUserGroups() {
		
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		dbClient = new CouchDbClient(properties);
		
		// Get user created groups
		userCreatedGroups = userData.get("joinedGroups").getAsString().split(",");
		
		for (String group : userCreatedGroups) {
			try {
				// Get data of group
				JsonObject groupData = dbClient.find(JsonObject.class, group);
				String[] usersInGroup = groupData.get("groupMembers").getAsString().split(",");
				
				// Go through list of users in group
				for (String usersString : usersInGroup) {
					AddWithoutDuplicates(usersString);
				}
				
				// Remove group messages
				removeGroupMessages(group);
				
				// Delete group from DB
				dbClient.remove(groupData);
			} catch (Exception e) {
				System.out.println("No such group in database.");
				continue;
			}
		}
		
		return;
	}
	
	
	private void removeGroupMessages(String group) {
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("chat")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		dbClient = new CouchDbClient(properties);
		
        // retrieve all data from couchdb instance
 		List<JsonObject> allChats = dbClient.view("_all_docs").includeDocs(true).query(JsonObject.class);
 		
 		for (JsonObject chat : allChats) {
			
			if (chat.get("group_ID").getAsString().equals(group)) {
				dbClient.remove(chat);
			}
		}
 		
		properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		dbClient = new CouchDbClient(properties);
	}
	
	private boolean checkRights(Cookie[] cookie_jar) {
		String cookieString = "";
		boolean isLoggedIn = false;
		
		// Check to see if any cookies exists
		if (cookie_jar == null) {
			// Redirect
			System.out.println("No cookie in browser.");
			return false;
		}
		
		for (int i =0; i< cookie_jar.length; i++) {
			Cookie aCookie = cookie_jar[i];
			
			if (aCookie.getName().equals("R3GISTRA_access")) {	
				cookieString = aCookie.getValue();
				isLoggedIn = true;
				break;
			}
		}
		
		if (!isLoggedIn) {
			// Redirect
			System.out.println("No cookie found.");
			return false;
		}


		System.out.println("Cookie value: " + cookieString);
		String[] cookieData = cookieString.split("\\|");
		
		// Check if user is logged in
		try {
			JsonObject user = dbClient.find(JsonObject.class, cookieData[0]);
			
			if (!user.get("accesstoken").getAsString().equals(cookieData[1])) {
				// Redirect
				System.out.println("Incorrect access token");
				return false;
			}
		}
		catch(NoDocumentException e) {
			// No such user
			System.out.println("No such user in database.");
			// Redirect
			return false;
		}
		
		System.out.println("Logged In.");
		
		return true;
	}
	
	private void AddWithoutDuplicates(String username) {
		boolean notInList = true;
		
		for (String userString : usersToUpdate) {
			
			if (userString.equals(username)) {
				notInList = false;
			}
		}
		
		// Add user to list
		if (notInList) {
			usersToUpdate.add(username);
		}
	}
}
