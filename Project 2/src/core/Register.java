package core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;

import com.google.gson.JsonObject;

import functions.EmailHandler;
import functions.RandomStringGenerator;


/**
 * Servlet implementation class Register
 */
@WebServlet(
		urlPatterns = { "/Register" }, 
		initParams = { 
				@WebInitParam(name = "Register", value = "", description = "")
		})
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// Regex pattern for valid email
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	// coudb properties set to class path
	private static CouchDbClient dbClient = new CouchDbClient();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		String baseURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(1), request.getContextPath());
		
		out.println(baseURL);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();	
		String email = request.getParameter("email").toLowerCase();	
		
		// Check if valid email address using Regex
		if (email.matches(EMAIL_PATTERN)) {
			
			// Check if in dictionary
		
			// If in database
			if (dbClient.contains(request.getParameter("email"))) {
				out.println("You have already signed up");
			}
			else {
				EmailHandler handler = new EmailHandler();
				
				// Create a user JsonObject
				JsonObject newUser = new JsonObject();
				newUser.addProperty("_id", email);
				newUser.addProperty("password", new RandomStringGenerator().generateString(email + Math.random()));
				newUser.addProperty("confirmed", false);
				newUser.addProperty("timestamp", "");
				newUser.addProperty("accesstoken", "");
				newUser.addProperty("joinedGroups", "");
				
				// Insert new user to DB
				dbClient.save(newUser);
				
				String token = dbClient.find(JsonObject.class, email).get("_rev").getAsString();
				String baseURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(1), request.getContextPath());

				// Registration message with link
				String registrationMessage = "Please click this link for to register: " + 
						baseURL + 
						"/Confirm?email=" + email +
						"&token=" + token;
				
				if (handler.sendEmail(email, "Chat Registration", registrationMessage)) {
					// Send confirmation
					out.println("Step 1 of registration complete. Please check your email.");
				}
				else {
					out.println("Error. Please try again");
				}
				
			}
			
		}
		else {
			out.println("Please enter a valid email.");
		}
	}
	

}

