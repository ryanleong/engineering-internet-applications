package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class Chat
 */
@WebServlet(
		urlPatterns = { "/Chat" }, 
		initParams = { 
				@WebInitParam(name = "Chat", value = "")
		})
public class Chat extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static ArrayList<JsonObject> chatLog = new ArrayList<JsonObject>();
	
	private CouchDbClient databaseClient;
	static int index = 0;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Chat() {
        super();
        
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("chat")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
        
        databaseClient = new CouchDbClient(properties);
        
        // retrieve all data from couchdb instance
 		List<JsonObject> dblog = databaseClient.view("_all_docs").includeDocs(true)
 				.query(JsonObject.class);
 		
 		// allocates extra space
 		chatLog.ensureCapacity(dblog.size());

 		// build all data in sequential order
 		for (int i = 0; i < dblog.size(); i++) {
 				
			chatLog.add(dblog.get(i)); // inefficient I know
			
			if (dblog.get(i).get("index").getAsInt() > index) { // set index
				index = dblog.get(i).get("index").getAsInt();
			}

 		}
 		
 		if (index > 1) {
 			sortchatLog();
 		}

 		index = chatLog.size() - 1;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int currentIndex;
		String tempIndexString = request.getParameter("index");
		
		if (tempIndexString == null) {
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
		else {
			currentIndex = Integer.parseInt(tempIndexString);
		}
		
		String group = request.getParameter("groupID");

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		JsonArray list = new JsonArray();

		for (int i = 0; i < chatLog.size(); i++) {

			if (chatLog.get(i).get("group_ID").getAsString().equals(group)) {
				if (i > currentIndex || currentIndex == 0) {
					list.add(chatLog.get(i));
				}
			}
		}

		out.println(list);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		response.setContentType("application/json");

		String name = request.getParameter("name");
		String message = request.getParameter("message");
		String group = request.getParameter("groupID");

		// Check all rights
		if (!checkRights(request.getCookies(), group)) {
			System.out.println("No rights to post to group " + group);
			
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
		
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("chat")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		JsonObject jsonObject = new JsonObject();

		index++;
		jsonObject.addProperty("index", index);
		jsonObject.addProperty("name", name);
		jsonObject.addProperty("message", message);
		jsonObject.addProperty("datetime", dateFormat.format(date));
		jsonObject.addProperty("_id", "" + index);
		jsonObject.addProperty("group_ID", group);

		chatLog.add(jsonObject);
		databaseClient.save(jsonObject);

		PrintWriter out = response.getWriter();
		out.println(jsonObject);
	}
	
	protected void sortchatLog() { // implementing selection sort
		int min, i, j;

		for (i = 0; i < chatLog.size(); i++) {
			min = i;

			for (j = i + 1; j < chatLog.size(); j++) {

				if (chatLog.get(j).get("index").getAsInt() < chatLog.get(min)
						.get("index").getAsInt()) {
					min = j;
				}
			}
			if (min != i) {
				swap(i, min);
			}
		}
	}
	
	protected void swap(int a, int b) { // straight swap function
		JsonObject tmp = new JsonObject();
		tmp = chatLog.get(a);
		chatLog.set(a, chatLog.get(b));
		chatLog.set(b, tmp);
	}
	
	private boolean checkRights(Cookie[] cookie_jar, String group) {
		String cookieString = "";
		boolean isLoggedIn = false;
		
		// Check to see if any cookies exists
		if (cookie_jar == null) {
			// Redirect
			System.out.println("No cookie in browser.");
			return false;
		}
		
		for (int i =0; i< cookie_jar.length; i++) {
			Cookie aCookie = cookie_jar[i];
			
			if (aCookie.getName().equals("R3GISTRA_access")) {	
				cookieString = aCookie.getValue();
				isLoggedIn = true;
				break;
			}
		}
		
		if (!isLoggedIn) {
			// Redirect
			System.out.println("No cookie found.");
			return false;
		}
		
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("users")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);

		System.out.println("Cookie value: " + cookieString);
		String[] cookieData = cookieString.split("\\|");
		
		// Check if user is logged in
		try {
			JsonObject user = databaseClient.find(JsonObject.class, cookieData[0]);
			
			if (!user.get("accesstoken").getAsString().equals(cookieData[1])) {
				// Redirect
				System.out.println("Incorrect access token");
				return false;
			}
		}
		catch(NoDocumentException e) {
			// No such user
			System.out.println("No such user in database.");
			// Redirect
			return false;
		}
		
		System.out.println("Logged In.");
		
		// Check if user is in group
		properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
  
		databaseClient = new CouchDbClient(properties);
		
		try {
			JsonObject groupDetails = databaseClient.find(JsonObject.class, group);
			
			System.out.println("Group Members: " + groupDetails.get("groupMembers").getAsString());
			
			String[] groupMembers = groupDetails.get("groupMembers").getAsString().split(",");
			
			boolean inGroup = false;
			for (String member : groupMembers) {
				
				// Check if group member
				if (member.trim().equals(cookieData[0].trim())) {
					inGroup = true;
					break;
				}
			}
			
			if (!inGroup) {
				// Redirect
				System.out.println("User not in group.");
				return false;
			}
			
		}
		catch (NoDocumentException e) {
			// No such group
			System.out.println("No such group.");
			// redirect
			return false;
		}
		
		System.out.println("Is Owner.");
		
		return true;
	}
}
