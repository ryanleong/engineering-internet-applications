package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@WebServlet(urlPatterns = { "/ChatServlet" }, initParams = { @WebInitParam(name = "ChatServlet", value = "", description = "ChatServlet") })
public class ChatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static ArrayList<JsonObject> chatLog = new ArrayList<JsonObject>();
	private int index = 0;

	// coudb properties set to class path
	private static CouchDbClient databaseClient;

	public void init(ServletConfig config) throws ServletException {

		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("chats")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("example.com")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		
		databaseClient = new CouchDbClient(properties);
		
		// retrieve all data from couchdb instance
		List<JsonObject> dblog = databaseClient.view("_all_docs").includeDocs(true)
				.query(JsonObject.class);
		
		// allocates extra space
		chatLog.ensureCapacity(dblog.size());

		// build all data in sequential order
		for (int i = 0; i < dblog.size(); i++) {
			if (dblog.get(i).get("keyValue").getAsString()
					.equalsIgnoreCase("rj-chat-data")) {
				
				chatLog.add(dblog.get(i)); // inefficient I know
				
				if (dblog.get(i).get("index").getAsInt() > index) { // set index
					index = dblog.get(i).get("index").getAsInt();
				}
			} else {
				System.err.println("found unknown doc entry");
			}
		}
		if (index > 1) {
			sortchatLog();
		}

		index = chatLog.size() - 1;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int currentIndex = Integer.parseInt(request.getParameter("index"));

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		JsonArray list = new JsonArray();

		for (int i = 0; i < chatLog.size(); i++) {

			if (i > currentIndex || currentIndex == 0) {
				list.add(chatLog.get(i));
			}
		}

		out.println(list);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		response.setContentType("application/json");

		String name = request.getParameter("name");
		String message = request.getParameter("message");

		JsonObject jsonObject = new JsonObject();

		index++;
		jsonObject.addProperty("index", index);
		jsonObject.addProperty("name", name);
		jsonObject.addProperty("message", message);
		jsonObject.addProperty("datetime", dateFormat.format(date));
		jsonObject.addProperty("_id", "" + index);
		jsonObject.addProperty("keyValue", "rj-chat-data");

		chatLog.add(jsonObject);
		databaseClient.save(jsonObject);

		PrintWriter out = response.getWriter();
		out.println(jsonObject);
	}

	protected void sortchatLog() { // implementing selection sort
		int min, i, j;

		for (i = 0; i < chatLog.size(); i++) {
			min = i;

			for (j = i + 1; j < chatLog.size(); j++) {

				if (chatLog.get(j).get("index").getAsInt() < chatLog.get(min)
						.get("index").getAsInt()) {
					min = j;
				}
			}
			if (min != i) {
				swap(i, min);
			}
		}
	}

	protected void swap(int a, int b) { // straight swap function
		JsonObject tmp = new JsonObject();
		tmp = chatLog.get(a);
		chatLog.set(a, chatLog.get(b));
		chatLog.set(b, tmp);
	}
}