package core;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Remove;
import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class DeleteGroup
 */
@WebServlet(
		urlPatterns = { "/DeleteGroup" }, 
		initParams = { 
				@WebInitParam(name = "DeleteGroup", value = "")
		})
public class DeleteGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private CouchDbClient databaseClient;
	private JsonObject groupDetails = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteGroup() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String group = request.getParameter("chatgroupID");
		
		if (group == null) {
			//Redirect
			System.out.println("Group ID is null");
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
		}
		
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("users")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		// Check all rights
		if (!checkRights(request.getCookies(), group)) {
			System.out.println("No rights to delete group " + group);
			
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}

		// Remove group details from each user
		removeGroupDataFromUser(group);
		
		// Remove groups from database
		removeGroup(group);
		
		// Remove chat messages from DB
		removeGroupMessages(group);
		
		// Redirect to home
		RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
		RD.forward(request, response);
	}
	
	private void removeGroupMessages(String group) {
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("chat")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
        // retrieve all data from couchdb instance
 		List<JsonObject> allChats = databaseClient.view("_all_docs").includeDocs(true).query(JsonObject.class);
 		
 		for (JsonObject chat : allChats) {
			
			if (chat.get("group_ID").getAsString().equals(group)) {
				databaseClient.remove(chat);
			}
		}
	}
	
	private boolean removeGroupDataFromUser(String deletingGroup) {
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		
		// Get memebers of group
		String[] groupMembers = groupDetails.get("groupMembers").getAsString().split(",");

		
		properties = new CouchDbProperties()
		  .setDbName("users")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		// Go through all members
		for (String memberID : groupMembers) {
			
			JsonObject userData;
			
			System.out.println("Processing group member: " + memberID);
			
			try {
				 userData = databaseClient.find(JsonObject.class, memberID);
			}
			catch (NoDocumentException e) {
				System.out.println("No such group member in group.");
				continue;
			}
			
			
			System.out.println("Groups user is in" + userData.get("joinedGroups").getAsString());
			
			String[] uservJoinedGroups = userData.get("joinedGroups").getAsString().split(",");
			String updatedUserJoinedGroups = "";
			
			for (String joinedGroup : uservJoinedGroups) {
				
				if (!joinedGroup.equals(deletingGroup)) {
					updatedUserJoinedGroups += joinedGroup + ",";
				}
			}
			
			System.out.println("Updated Joined Groups: " + updatedUserJoinedGroups);
			
			// Update user data
			userData.addProperty("joinedGroups", updatedUserJoinedGroups);
			databaseClient.update(userData);
		}
		
		return true;
	}
	
	private void removeGroup(String group) {
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		try {
			JsonObject groupData = databaseClient.find(JsonObject.class, group);
			databaseClient.remove(groupData);
		} catch (Exception e) {
			System.out.println("No such group in database.");
			return;
		}
		
	}


	private boolean checkRights(Cookie[] cookie_jar, String group) {
		String cookieString = "";
		boolean isLoggedIn = false;
		
		// Check to see if any cookies exists
		if (cookie_jar == null) {
			// Redirect
			System.out.println("No cookie in browser.");
			return false;
		}
		
		for (int i =0; i< cookie_jar.length; i++) {
			Cookie aCookie = cookie_jar[i];
			
			if (aCookie.getName().equals("R3GISTRA_access")) {	
				cookieString = aCookie.getValue();
				isLoggedIn = true;
				break;
			}
		}
		
		if (!isLoggedIn) {
			// Redirect
			System.out.println("No cookie found.");
			return false;
		}


		System.out.println("Cookie value: " + cookieString);
		String[] cookieData = cookieString.split("\\|");
		
		// Check if user is logged in
		try {
			JsonObject user = databaseClient.find(JsonObject.class, cookieData[0]);
			
			if (!user.get("accesstoken").getAsString().equals(cookieData[1])) {
				// Redirect
				System.out.println("Incorrect access token");
				return false;
			}
		}
		catch(NoDocumentException e) {
			// No such user
			System.out.println("No such user in database.");
			// Redirect
			return false;
		}
		
		System.out.println("Logged In.");
		
		// Check if user is owner of group
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
  
		databaseClient = new CouchDbClient(properties);
		
		
		try {
			groupDetails = databaseClient.find(JsonObject.class, group);
			
			// Check if group owner
			if (!groupDetails.get("groupOwner").getAsString().equals(cookieData[0])) {
				// Redirect
				System.out.println("User not group owner");
				return false;
			}
		}
		catch (NoDocumentException e) {
			// No such group
			System.out.println("No such group.");
			// redirect
			return false;
		}
		
		System.out.println("Is Owner.");
		
		
		return true;
	}
	
}
