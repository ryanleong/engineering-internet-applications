package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;

import com.google.gson.JsonObject;

import functions.EmailHandler;
import functions.RandomStringGenerator;
/**
 * Servlet implementation class Reminder
 */
@WebServlet(
		urlPatterns = { "/Reminder" }, 
		initParams = { 
				@WebInitParam(name = "Reminder", value = "")
		})
public class Reminder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static CouchDbClient dbClient = new CouchDbClient();   
    private static long reminderInterval = 3600*1000; 
	/**
     * @see HttpServlet#HttpServlet()
     */
    public Reminder() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();	
		long currentTime = System.currentTimeMillis();
		String email = request.getParameter("email").toLowerCase();
		// Check if valid email
		//System.out.println(email);	
		if (dbClient.contains(email)) {
			JsonObject userData = dbClient.find(JsonObject.class, email);
			String password = userData.get("password").getAsString();
			long timeStamp = userData.get("timestamp").getAsLong();
			
			// Send email with password (Same as "Confirm" servlet)
			if (!userData.get("confirmed").getAsBoolean()){
				out.println("usser not comfirmed yet");
			}
			else if (currentTime > (timeStamp + reminderInterval)){
				new EmailHandler().sendEmail(email, "Chat Password", 
						("Your chat password is:  " + password));
				userData.addProperty("timestamp", System.currentTimeMillis());
				dbClient.update(userData);
				// User is redirected to a confirmation screen
				out.println("your password has been sent to your email");
				//response.sendRedirect("/Email_Registration_System");					
			
			} else {
			out.println("please refer to last email, alternatively wait 5 minutes");
			}
		} else {
			out.println("no user registered with that email");
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
