package core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.util.StringParser;
import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class LeaveGroup
 */
@WebServlet(
		urlPatterns = { "/LeaveGroup" }, 
		initParams = { 
				@WebInitParam(name = "LeaveGroup", value = "")
		})
public class LeaveGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private CouchDbClient databaseClient;
	private JsonObject groupDetails = null;
	private String userID = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LeaveGroup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String group = request.getParameter("groupName");
		
		if (group == null) {
			//Redirect
			System.out.println("Group ID is null");
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
		}
		
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("users")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		// Check all rights
		if (!checkRights(request.getCookies(), group)) {
			System.out.println("No rights to delete group " + group);
			
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}

		String[] groupMembers = groupDetails.get("groupMembers").getAsString().split(",");
		String updatedGroupMembers = "";
		
		for (String member : groupMembers) {
			
			if (!member.equals(userID)) {
				updatedGroupMembers += member + ",";
			}
		}
		
		// Update group details
		groupDetails.addProperty("groupMembers", updatedGroupMembers);
		databaseClient.update(groupDetails);
		
		properties = new CouchDbProperties()
		  .setDbName("users")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
		databaseClient = new CouchDbClient(properties);
		
		JsonObject user = databaseClient.find(JsonObject.class, userID);
		String[] usersGroups = user.get("joinedGroups").getAsString().split(",");
		String updatedUserGroups = "";
		
		for (String joinedGroup : usersGroups) {
			
			if (!joinedGroup.equals(group)) {
				updatedGroupMembers += joinedGroup + ",";
			}
		}
		
		// Update user
		user.addProperty("joinedGroups", updatedUserGroups);
		databaseClient.update(user);
		
		PrintWriter out = response.getWriter();
		out.println("DONE");
	}


	private boolean checkRights(Cookie[] cookie_jar, String group) {
		String cookieString = "";
		boolean isLoggedIn = false;
		
		// Check to see if any cookies exists
		if (cookie_jar == null) {
			// Redirect
			System.out.println("No cookie in browser.");
			return false;
		}
		
		for (int i =0; i< cookie_jar.length; i++) {
			Cookie aCookie = cookie_jar[i];
			
			if (aCookie.getName().equals("R3GISTRA_access")) {	
				cookieString = aCookie.getValue();
				isLoggedIn = true;
				break;
			}
		}
		
		if (!isLoggedIn) {
			// Redirect
			System.out.println("No cookie found.");
			return false;
		}


		System.out.println("Cookie value: " + cookieString);
		String[] cookieData = cookieString.split("\\|");
		userID = cookieData[0];
		
		// Check if user is logged in
		try {
			JsonObject user = databaseClient.find(JsonObject.class, cookieData[0]);
			
			if (!user.get("accesstoken").getAsString().equals(cookieData[1])) {
				// Redirect
				System.out.println("Incorrect access token");
				return false;
			}
		}
		catch(NoDocumentException e) {
			// No such user
			System.out.println("No such user in database.");
			// Redirect
			return false;
		}
		
		System.out.println("Logged In.");
		
		// Check if user is owner of group
		CouchDbProperties properties = new CouchDbProperties()
		  .setDbName("groups")
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);
  
		databaseClient = new CouchDbClient(properties);
		
		try {
			groupDetails = databaseClient.find(JsonObject.class, group);
			
			// Check if group owner
			if (groupDetails.get("groupOwner").getAsString().equals(cookieData[0])) {
				// Redirect
				System.out.println("User is group owner");
				return false;
			}
		}
		catch (NoDocumentException e) {
			// No such group
			System.out.println("No such group.");
			// redirect
			return false;
		}
		
		System.out.println("Is Owner.");
		
		
		return true;
	}
}
