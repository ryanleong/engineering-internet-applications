package functions;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailHandler {
        final private String username = "einterneta17@gmail.com";
        final private String password = "hello world";
        //final private String username = "jwilkosz@student.unimelb.edu.au";
        //final private String password = "";
		
		private Properties props = new Properties();
        private Session session;
        
        public EmailHandler() {
                // Set up mailing properties
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.port", "587");
                //props.put("mail.smtp.host", "localhost");
                //props.put("mail.smtp.port", "25");
                
                
                // Set up session
                session = Session.getInstance(props,
                                new javax.mail.Authenticator() {
                                        protected PasswordAuthentication getPasswordAuthentication() {
                                                return new PasswordAuthentication(username, password);
                                        }
                                });
        }
        
        public boolean sendEmail(String email, String subject, String data) {

                try {

                        Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress("user_registration@gmail.com"));
                        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
                        
                        message.setSubject(subject);
                        message.setText(data);

                        Transport.send(message);

                        return true;

                } catch (MessagingException e) {
                        // throw new RuntimeException(e);
                        return false;
                }
        }
}