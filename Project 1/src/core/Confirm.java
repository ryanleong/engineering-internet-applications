package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;

import com.google.gson.JsonObject;

import functions.EmailHandler;
import functions.RandomStringGenerator;

/**
 * Servlet implementation class Confirmation
 */
@WebServlet(
		urlPatterns = { "/Confirm" }, 
		initParams = { 
				@WebInitParam(name = "Confirm", value = "", description = "")
		})
public class Confirm extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	// coudb properties set to class path
	private static CouchDbClient dbClient = new CouchDbClient();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Confirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		// Get email and token from confirmation link
		String email = request.getParameter("email").toLowerCase();
		String token = request.getParameter("token");
		
		// Get client data
		JsonObject userData = dbClient.find(JsonObject.class, email);
		
		// Check if token is database
		String tokenString = userData.get("_rev").getAsString();
		
		if (!token.equals(tokenString)) {
			// Error message
			out.println("Invalid confirmation link.");
			return;
		}
		
		// Generate Random Password
		String randomPassword = new RandomStringGenerator().generateString(email+token);
		
		// If Generation failed
		if (randomPassword == null) {
			System.err.println("Failed to generated random password. Processes aborted.");
			return;
		}
    
		// Update database with random password
		userData.addProperty("password", randomPassword);
		userData.addProperty("confirmed", true);
		userData.addProperty("timestamp", System.currentTimeMillis());
		dbClient.update(userData);
		
		// Password email is sent to user with the password
		new EmailHandler().sendEmail(email, "Chat Password", 
				("Your chat password is:  " + randomPassword));
	
		// User is redirected to a confirmation screen
		out.println("Your password has bee sent to your email");
		
		//response.sendRedirect("/Email_Registration_System");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
