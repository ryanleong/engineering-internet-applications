package core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class Logout
 */
@WebServlet(
		urlPatterns = { "/Logout" }, 
		initParams = { 
				@WebInitParam(name = "Logout", value = "")
		})
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	// Connection to DB
	private static CouchDbClient dbClient = new CouchDbClient();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Logout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	
		// make sure to disqualify the cookie as reload attemp will allow a valid session
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		// Get Token
		String token = request.getParameter("token");
		String email = request.getParameter("email").toLowerCase();
		
		// Get client data
		JsonObject userData = dbClient.find(JsonObject.class, email);
		String tokenString = userData.get("accesstoken").getAsString();
		
		// Check token to see if logged in
		if (tokenString.equals(token)) {
			try {
				
				// Reset accesstoken in DB
				userData.addProperty("accesstoken", "");
				dbClient.update(userData);
				
				out.println("TRUE");
			} catch (Exception e) {
				out.println("ERROR");
			}
		}
		else {
			out.println("FALSE");
		}
		
	}

}
