  package core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.lightcouch.NoDocumentException;

import com.google.gson.JsonObject;

import functions.RandomStringGenerator;

/**
 * Servlet implementation class Login
 */
@WebServlet(
		urlPatterns = { "/Login" }, 
		initParams = { 
				@WebInitParam(name = "Login", value = "")
		})
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// coudb properties set to class path
	private static CouchDbClient dbClient = new CouchDbClient();
    private static int PROP_cookieExp = 3600; //cookie expiery    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/Email_Registration_System");
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String baseURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(1), request.getContextPath());
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		// Get email and password from confirmation link
		String email = request.getParameter("email").toLowerCase();
		String password = request.getParameter("password");
		
		String passwordDB;
		
		//IO information variable
		JsonObject redirect = new JsonObject(); //add details for successfull loginfurther built upon in CookieServlet
		redirect.addProperty("valid",false);
		redirect.addProperty("email",false);
		redirect.addProperty("location","home.jsp");
		redirect.addProperty("confirmed", false);
		
		if(dbClient.contains(email)){
		try {
			JsonObject userData = dbClient.find(JsonObject.class, email);
			redirect.addProperty("email",email);
			// Check if email and password are in DB
			passwordDB = userData.get("password").getAsString();
			if (userData.get("confirmed").getAsBoolean()) {
				redirect.addProperty("confirmed",userData.get("confirmed").getAsBoolean());	
				if (password.equals(passwordDB)) {
					redirect.addProperty("valid", true);
					// Return access token
					String accessToken = new RandomStringGenerator().generateString(new Random().nextInt(10000) + "");
					// Update database with random password
					userData.addProperty("accesstoken", accessToken);
					dbClient.update(userData);
				
					// Store token in browser
					Cookie cookie = new Cookie("R3GISTRA_access", (userData.get("_id").getAsString() + "|" + accessToken));
					cookie.setMaxAge(PROP_cookieExp);
					response.addCookie(cookie);
				
					out.println(redirect);
				} else {
					out.println(redirect);
				}
			} else {
				// Reply with notification
				out.println(redirect);
			} 
		} catch (NoDocumentException e) {
			// TODO: handle exception
			//out.println("Not in DB");
		}
		} else {
		out.println(redirect);
		}
	}
}
