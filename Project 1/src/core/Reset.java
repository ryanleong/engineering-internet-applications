package core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;

import com.google.gson.JsonObject;

import functions.EmailHandler;
import functions.RandomStringGenerator;

/**
 * Servlet implementation class Reset
 */
@WebServlet(
		urlPatterns = { "/Reset" }, 
		initParams = { 
				@WebInitParam(name = "Reset", value = "")
		})
public class Reset extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// Connection to DB
	private static CouchDbClient dbClient = new CouchDbClient();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Reset() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		// Get Token
		String token = request.getParameter("token");
		String email = request.getParameter("email");
		
		if (token == null || email == null) {
			RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
			RD.forward(request, response);
			return;
		}
		
		// Get client data
		JsonObject userData = dbClient.find(JsonObject.class, email);
		String tokenString = userData.get("accesstoken").getAsString();
		
		// Check token to see if logged in
		if (tokenString.equals(token)) {
			try {
				// Generate Random Password
				String randomPassword = new RandomStringGenerator().generateString(email + Math.random());
				
				// If Generation failed
				if (randomPassword == null) {
					System.err.println("Failed to generated random password. Processes aborted.");
					return;
				}
				
				// Update database with random password
				userData.addProperty("password", randomPassword);
				userData.addProperty("timestamp", System.currentTimeMillis());
				// TODO reset token
				userData.addProperty("accesstoken", "");
				dbClient.update(userData);
				
				// Password email is sent to user with the password
				new EmailHandler().sendEmail(email, "New Chat Password", 
						("Your new chat password is:  " + randomPassword));
				
				out.println("reset password, redirecting...");
			} catch (Exception e) {
				out.println("Unable to reset your password");
			}
		}
		else {
			out.println("You're not logged in");
		}
	}

}
