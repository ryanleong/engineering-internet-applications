package core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class Delete
 */
@WebServlet(
		urlPatterns = { "/Delete" }, 
		initParams = { 
				@WebInitParam(name = "Delete", value = "")
		})
public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// Connection to DB
	private static CouchDbClient dbClient = new CouchDbClient();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		// Get Token
		String token = request.getParameter("token");
		String email = request.getParameter("email").toLowerCase();
		
		// Get client data
		JsonObject userData = dbClient.find(JsonObject.class, email);
		String tokenString = userData.get("accesstoken").getAsString();
		// Check token to see if logged in
		if (tokenString.equals(token)) {
			try {
				// Delete account
				dbClient.remove(userData);
				out.println("TRUE");
			} catch (Exception e) {
				out.println("ERROR");		
			}
		}
		else {
			out.println("FALSE");
		}
	}

}
