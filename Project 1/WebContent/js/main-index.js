// js for index.jsp

//global variables
var useremail;

//size window functions and build alert dialog
$(document).ready(function(){
	$("#R3G-body").height( ($(window).height() - $("#R3G-navbar").height() - $("#R3G-body").height() - $("#R3G-footer").height() -30));
	$("#dialog").dialog({
	autoOpen: false,
	modal: true,
		});
	});
$(window).resize(function(){
	//$("#R3G-body").height( ($(window).height() - $("#R3G-navbar").height() - $("#R3G-body").height() - $("#R3G-footer").height() -30));
});
//general functions for use with functional regiastral services 
function signupform(){
	$('#alertregister').html("<div id='regiterform' class='input-group'>"+
							 "<form id='register' action='javascript:register()' method='post'>"+
							 "<input id='emailregister' style='margin-top:-2px;' type='text' name='email' size='30' placeholder='Email Address'/>"+	
							 "<input type='submit' name='submit' style='display: none'/>"+
							 "</form></div>");
	
	$("#emailregister").focus();
}
function init(){
	$("#emaillogin").focus();
	$('#alertregister').html("");
	
}
function login() {
	var datastr = 'email=' + $('#emaillogin').val() + "&password=" + $("#passwordlogin").val();
	$('#alertlogin').html("<h8>loading...<h8>");
	
	if(!validateEmail($('#emaillogin').val())) {
		$('#alertlogin').html("");
		$('#alertlogin').html("<h8>Not a valid email<h8>");
		// Set Dialog message
		//$('#dialog').html("Please enter valid email!");
		
		// Display dialog on click
		//$( "#dialog" ).dialog("open");
	}
	else {
		$.ajax({	
			type : "post",
			url : "Login",
			data : datastr,
			dataType : "json",
			success : function(msg) {
			    if(msg.email==false){
				     $('#alertlogin').html("<span><h8>Not a registered user </h8>");	
			    } else if (!msg.confirmed) {
					$('#alertlogin').html("<h8>User not confirmed <br />Check email<h8>");
				}else if(msg.valid) {
					$('#alertlogin').html("<h8>Redirecting...<h8>");
					 window.location = msg.location;
		        } else {
					$('#alertlogin').html("<span><h8> Wrong password, send </h8>" +
							              "<a href='javascript:reminder()'><h8>reminder?</h8></a>");
					useremail = msg.email;
				}
			}
		});
	}
	//return false;
}
function register(){
	var datastr = 'email=' + $('#emailregister').val();
	
	if(!validateEmail($('#emailregister').val())) {
		// Set Dialog message
		$('#dialog').html("Please enter valid email!");
		
		// Display dialog on click
		$( "#dialog" ).dialog("open");
	}
	else {
		// clear user data indicate loading
		$("#emailregister").val("");
		$("#emailregister").attr("placeholder","loading...");
		$.ajax({
			type : "post",
			url : "Register",
			data : datastr,
			success : function(msg) {
				// Set Dialog message
				$('#dialog').html(msg);
				// Display dialog on click
				$( "#dialog" ).dialog("open");
				$('#alertregister').html("");
			}
		});	
	}
	//return false;
}        
function validateEmail(email){
	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	var valid = emailReg.test(email);

	if(!valid) {
        return false;
    } else {
    	return true;
    }
}
function reminder(){
	$('#alertlogin').html("<h8>processing reminder, please wait...<h8>");
	$.ajax({	
		type : "get",
		url : "Reminder",
		data : "email="+useremail,
		success : function(msg) {
				$('#alertlogin').html("<span><h8>"+msg+"</h8>");
			}
		});
}