<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--  FIND COOKIE INFO -->
<%@ page import="com.google.gson.JsonObject" import="java.util.io.*" 
import="javax.servlet.RequestDispatcher"
import="javax.servlet.ServletException"
import="javax.servlet.annotation.WebInitParam"
import="javax.servlet.annotation.WebServlet"
import="javax.servlet.http.HttpServlet"
import="javax.servlet.http.HttpServletRequest"
import="javax.servlet.http.HttpServletResponse" 
import="org.lightcouch.CouchDbClient"
import="org.lightcouch.NoDocumentException"
%>
<%  CouchDbClient dbClient = new CouchDbClient();
	String access = "R3GISTRA_access";  		//changed cookie name for deliminator
	JsonObject USER = new JsonObject();         //object to use for response
	USER.addProperty("valid", false);
	USER.addProperty("email","");
	javax.servlet.http.Cookie[] cookies = request.getCookies(); 
	 
	for (int i=0;i<(cookies==null?0:cookies.length);i++){ //checks for null cookies (if zero) 
	javax.servlet.http.Cookie cookie = cookies[i];    
	if(access.equals(cookie.getName())) {
		try {
			String token = cookie.getValue().trim();
			String [] tokens = token.split("\\|");
			//build Json data - maybe used for sessions data
			JsonObject userData = dbClient.find(JsonObject.class, tokens[0]);
			//check if cookie is valid		
			if ((userData.get("accesstoken").getAsString()).equals(tokens[1])){
			    USER.addProperty("valid",true);
			    USER.addProperty("email",tokens[0]);
			    RequestDispatcher RD = request.getRequestDispatcher("home.jsp");
				RD.forward(request, response);
				} 
		} catch (NoDocumentException e) {
			// TODO: handle exception
			System.err.println("error user no longer in couchdb");
			} 
		} 
	}
%>
<!--  /FIND COOKIE INFO -->

<html>
<head>
<title>R3GISTRA | LAUNCH PAGE</title>

	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script type="text/javascript" src="jquery/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="jquery/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main-index.js"></script>
    <script src="js/main.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
</head>
<body>
	<div id="R3G-navbar" class="navbar R3G-nav">
		<div class="container">
			<div class="navbar-header">
			<a class="navbar-brand R3G-link" href="index.jsp">R3GISTRA</a>
			</div>  
			<div class="">
        		<ul class="nav navbar-nav">
 
         		 
          		<li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" onclick="init()">Sign In<strong class="caret"></strong></a>
            	<div id="signup" class="dropdown-menu" style="padding: 15px; padding-bottom: 10px;">
              		<form id="mytlogin" method="get" action="javascript:login()">
  			    	<input id="emaillogin" style="margin-bottom: 15px;" type="text" name="email" size="30" placeholder="Username"/>
  					<input id="passwordlogin" style="margin-bottom: 15px;" type="password" name="password" size="30" placeholder="Password"/>
  					<input class="btn btn-primary" style="clear: left; width: 100%; height: 32px; font-size: 13px;" type="submit" name="commit" value="Sign In" />	
					</form>
					<div id="alertlogin"></div>
            	</div>
         		 </li>
         		 <li class="divider-vertical"></li>
         	<li><a href="javascript:signupform()">Sign Up</a></li>
         		 <li class=""><div id="alertregister" style="margin-top:15px;"></div></li>
         		 
         		 
        		</ul>
			</div>
		</div>
	</div>
	<div id="R3G-body" class="container-fluid">
		<div class="container">
			<div class="row">
			</div>
		</div>
	</div>
	<div id="R3G-footer" class="container-fluid">
		<div class="container">
			<div class="row">
				<span class="R3G-container R3G-footer">&#169;2013&nbsp;|&nbsp;<a href="http://www.ryanleong.me" class=".rj-credits-dev-link" target="_blank">ryanleong</a>&nbsp;|&nbsp; 
				<a href="http://www.wilkosz.com.au" class=".rj-credits-dev-link" target="_blank">joshuawilkosz</a></span> 
			</div>
		</div>
	</div>
	<div id="dialog" title="Alert"></div>
</body>
<script>
$(function() {
	  // Setup drop down menu
	  $('.dropdown-toggle').dropdown();
	 
	  // Fix input element click problem
	  $('.dropdown input, .dropdown label').click(function(e) {
		  e.stopPropagation();
	  });
	});
</script>
</html>












