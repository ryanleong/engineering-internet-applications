<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="com.google.gson.JsonObject" import="java.util.io.*" 
import="javax.servlet.RequestDispatcher"
import="javax.servlet.ServletException"
import="javax.servlet.annotation.WebInitParam"
import="javax.servlet.annotation.WebServlet"
import="javax.servlet.http.HttpServlet"
import="javax.servlet.http.HttpServletRequest"
import="javax.servlet.http.HttpServletResponse" 
import="org.lightcouch.CouchDbClient"
import="org.lightcouch.NoDocumentException"
%>
 
<!--  FIND COOKIE INFO -->
<%  CouchDbClient dbClient = new CouchDbClient();
	String access = "R3GISTRA_access";  		//changed cookie name for deliminator
	JsonObject USER = new JsonObject();     //object to use for response
	USER.addProperty("valid", false);
	USER.addProperty("email","");
	javax.servlet.http.Cookie[] cookies = request.getCookies(); 
	 
	for (int i=0;i<(cookies==null?0:cookies.length);i++){ //checks for null cookies (if zero) 
	javax.servlet.http.Cookie cookie = cookies[i];   
	if(access.equals(cookie.getName())) {
		try {
			String token = cookie.getValue().trim();
			String [] tokens = token.split("\\|");
			//build Json data - maybe used for sessions data
			JsonObject userData = dbClient.find(JsonObject.class, tokens[0]);
			//check if cookie is valid	
			if ((userData.get("accesstoken").getAsString()).equals(tokens[1])){
			    USER.addProperty("valid",true);
			    USER.addProperty("email",tokens[0]);
			    USER.addProperty("accesstoken", tokens[1]);
			} else {
				USER.addProperty("valid",false);
			}
		} catch (NoDocumentException e) {
			// TODO: handle exception
			System.err.println("error user no longer in couchdb");
			} 
		} 
	} 
	if (USER.get("valid").getAsBoolean()==false) {
		RequestDispatcher RD = request.getRequestDispatcher("index.jsp");
		RD.forward(request, response);
	}
%>
<!--  /FIND COOKIE INFO -->


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>R3GISTRA | <%= USER.get("email").getAsString() %></title>
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script type="text/javascript" src="jquery/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="jquery/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main-home.js"></script>
    <script src="js/main.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script> //jsp specific function calls 
		$(document).ready(function() {});
		function logout(){
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>	
			$.ajax({
				type : "post",
				url : "Logout",
				data : datastr,
				success : function(msg) {
					if(/^\s*TRUE\s*$/.test(msg)) {
						window.location = "index.jsp";
					}
					else if(/^\s*FALSE\s*$/.test(msg)) {
						$('#alertlogout').html("");
					}
					
				}
			});
		}
		function redirect(){
			window.location = "index.jsp";
		}
		function resetUser() {
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>
			$('#alertreset').html("<h8>resetting...<h8>");
			$.ajax({
				type : "post",
				url : "Reset",
				data : datastr,
				success : function(msg) {
					if (/^\s*success\s*$/.test(msg))
					$('#alertreset').html("<h8>" + msg + "<h8>");
					setTimeout(redirect, 2000);
				}
			});
		}
		function deleteUser() {
			var datastr = "token=" + <%= USER.get("accesstoken") %> + "&email=" + <%= USER.get("email") %>
			$('#alertreset').html("<h8>deletting...<h8>");
			$.ajax({
				type : "post",
				url : "Delete",
				data : datastr,
				success : function(msg) {
					if(/^\s*TRUE\s*$/.test(msg)) {
						window.location = "index.jsp";
					}
					else if(/^\s*FALSE\s*$/.test(msg)) {
						$('#alertlogout').html("<h8>You are not logged in.<h8>");
					}
				}
			});
		}
	</script>
</head>
<body>
	<div id="R3G-navbar" class="navbar R3G-nav">
		<div class="container">
			<div class="navbar-header">
			<a class="navbar-brand R3G-link" href="index.jsp">R3GISTRA |  <%= USER.get("email").getAsString() %></a>
			</div>  
			   <div class="">
        		<ul class="nav navbar-nav">	 
         		 <li><a href="javascript:logout()">Log Out</a></li>
         		 <li class=""><div id="alertlogout" style="margin-top:15px;"></div></li>
         		 <li class="divider-vertical"></li>
         		 <li><a href="javascript:resetUser()">Reset</a></li>
         		 <li class=""><div id="alertreset" style="margin-top:15px;"></div></li>
         		 <li class="divider-vertical"></li>
         		 <li><a href="javascript:deleteUser()">Delete</a></li>
         		 <li class=""><div id="alertdelet" style="margin-top:15px;"></div></li>
        		</ul>
			</div>
		</div>
	</div>
	<div id="R3G-body" class="container-fluid">
		<div class="container">
			<div class="row">
			</div>
		</div>
	</div>
	<div id="R3G-footer" class="container-fluid">
		<div class="container">
			<div class="row">
				<span class="R3G-container R3G-footer">&#169;2013&nbsp;|&nbsp;<a href="http://www.ryanleong.me" class=".rj-credits-dev-link" target="_blank">ryanleong</a>&nbsp;|&nbsp; 
				<a href="http://www.wilkosz.com.au" class=".rj-credits-dev-link" target="_blank">joshuawilkosz</a></span> 
			</div>
		</div>
	</div>
	<div id="dialog" title="Alert"></div>
</body>
</html>

 

