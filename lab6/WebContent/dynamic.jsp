<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page 
	import="java.net.*"
	import="java.util.io.*"
	import="javax.servlet.http.HttpServletRequest"
%>
<%! int tdwidth = 2; %>


<HTML>
<HEAD>
	<TITLE>Lab 6</TITLE>

	<link rel="stylesheet" type="text/css" href="css/main.css" />
</HEAD>

<BODY>
	<h2>Dynamic Table View</h2>
    <a href="table.jsp">View a simple table with text and thumbnails</a><br /><br />
    <a href="entry.jsp">Enter a different set of URLs</a><br /><br />
    
    <form method="post" action="dynamic.jsp">
    (0<) Table width (>16) = 
    <input type="TEXT" name="thiswidth" placeholder="<%= tdwidth %>" value="" size="2"/>
    <input type="submit" value="cahnge width"/>
    </form>
	<table>
	<%  // Read from session
		String urls = (String) session.getAttribute("urls");
		String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		String reg = "^+ ";
		if (urls != null) {
			String url[] = urls.split("\\r?\\n");
			// initialize width 
			if (request.getParameter("thiswidth")==null){
				
			} else {
				tdwidth = Integer.parseInt(request.getParameter("thiswidth"));
				
				}
			int i=0;	
			while (i<url.length){
				out.println("<tr>");	
				for(int j=0;j< tdwidth ;j++){
					if(i==url.length){
						continue;
						} else {
						String encodedurl = URLEncoder.encode(url[i].toString(),"UTF-8"); 
						out.println("<td>"+   
								"<a href='" + url[i] + "' target='new'>"+
						    	"<img src='http://beta.webthumbnail.org/?width=50&height=40&format=png&url="+
					        	encodedurl + "'></a></td>");	
						i++;
						}	
					}
				out.println("</tr>");
				}
			}
	%>
	</table>

</BODY>

</HTML>