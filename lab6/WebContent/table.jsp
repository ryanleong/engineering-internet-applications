<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page 
	import="java.net.*"
%>

<HTML>
<HEAD>
	<TITLE>Lab 6</TITLE>

	<link rel="stylesheet" type="text/css" href="css/main.css" />
</HEAD>

<BODY>
	<h2>Text and Thumbnail View</h2>
	<a href="dynamic.jsp">View a dynamic table of thumbnails</a><br /><br />
	 <a href="entry.jsp">Enter a different set of URLs</a>
	<table>
	
	<%
		// Read from session
		String urls = (String) session.getAttribute("urls");

		String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		String reg = "^+ ";
	
		if (urls != null) {
			String url[] = urls.split("\\r?\\n");
			
			for(int i = 0; i < url.length; i++) {
				String encodedurl = URLEncoder.encode(url[i].toString(),"UTF-8"); 
				
				out.println("<tr><td>" + url[i] + "</td>");
				
				out.println("<td>"
						+ "<a href='" + url[i] + "' target='new'>"
						+ "<img src='http://beta.webthumbnail.org/?width=50&height=40&format=png&url=" 
						+ encodedurl + "'></a></td></tr>");
			}
			
		}
	%>
	</table>

</BODY>

</HTML>