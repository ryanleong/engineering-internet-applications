<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page 
	
%>

<%
	// Get email and token from confirmation link
	String urls = request.getParameter("urlTextArea");

	if(urls != null) {
		session.setAttribute("urls", urls);
		response.sendRedirect("table.jsp");
	}

%>
<HTML>
<HEAD>
	<TITLE>Lab 6</TITLE>

	<link rel="stylesheet" type="text/css" href="css/main.css" />
</HEAD>

<BODY>

	<center>
		<h1>Enter list of websites</h1>
		<h5>One URL per line</h5>
		
		<form method="POST" action="entry.jsp">
			<textarea id="urlTextArea" name="urlTextArea" rows=15 cols=90>
http://www.yahoo.com
http://www.google.com
http://www.youtube.com
http://www.unimelb.edu.au
http://www.futurestudents.unimelb.edu.au/grad/</textarea> 
			<br />
			<input type="submit" value="Submit">
		</form>
	</center>


</BODY>

</HTML>