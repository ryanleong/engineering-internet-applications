$(document).ready(function(){});

function mywebsockets(){
	//alert("ws://"+ location.host + location.pathname + "myWebSocketServlet");
	$("#myresponse").html("connecting to "+location.host+"...");
	mywebsocket = new WebSocket("ws://"+ location.host + location.pathname + "myWebSocketServlet"); 
	mywebsocket.onopen = function(evt) { 
		$("#myresponse").html("connected...");
		mywebsocket.send($("#mydata").val()); 
		$("#mydata").val("");
		}; 
	mywebsocket.onclose = function(evt) {	
		setTimeout(function(){
			$("#myresponse").html("");
			$("#mydata").val("");
			},4000);        
		};
	mywebsocket.onmessage = function(evt) { 
		$("#myresponse").html("you stuttered: "+evt.data); 
		mywebsocket.close(); 
		}; 
	mywebsocket.onerror = function(evt) { 
		$("#myresponse").html("error: "+evt.data);     
		}; 
	window.addEventListener("load", init, false);
}
function websockets(){
	$("#echoresponse").html("connecting to echo.websocket.org...");
	websocket = new WebSocket("ws://echo.websocket.org/"); 
	websocket.onopen = function(evt) { onOpen(evt);      }; 
	websocket.onclose = function(evt) { onClose(evt);    };
	websocket.onmessage = function(evt) { onMessage(evt);}; 
	websocket.onerror = function(evt) { onError(evt);    }; 
	//functions from above
	function onOpen(evt) { 
		$("#echoresponse").html("connected..."); 
		doSend($("#echodata").val());
		$("#echodata").val("");
	}  
	function onClose(evt) { 
		setTimeout(function(){
			$("#echoresponse").html("");
			$("#echodata").val("");
			},4000);
	}  
	function onMessage(evt) { 
		$("#echoresponse").html("echo says: "+ evt.data); 
		websocket.close(); 
	}  
	function onError(evt) { 
		$("#echoresponse").html("error: "+evt.data); 
	}  
	function doSend(message) { 
		$("#echoresponse").html("sending: " + message);  
		websocket.send(message);
	}    
	window.addEventListener("load", init, false);
}
	