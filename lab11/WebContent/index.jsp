<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>WebSockets | TEST</title>
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
	<script src="js/main.js"></script>
</head>
<body>
<center><div style="border-bottom:dashed 1px black;width:400px;margin-top:50px;">WebSocket Testing Platform</div></center>
<center>
	<div id=websocket style="margin-top:20px;">
	<form id="echo" action="javascript:websockets()" method="post">
		<input id='echodata' style='margin-top:-2px;' type='text' name='email' size='60' placeholder="enter data to send to echo.webSockets.org"/>
		<input type='submit' name='submit' style='display: none'/>
	</form>
	<div id="echoresponse"></div>
	<form id="mywebsockets" action="javascript:mywebsockets()" method="post" style="padding-top:10px;">
		<input id='mydata' style="margin-top:-2px;" type='text' name='email' size='60' placeholder="localserver> play stutter: type something..."/>
		<input type="submit" name="submit" style="display: none"/>
	</form>
	<div id="myresponse"></div>
	</div>
</center>  
</body>
</html>