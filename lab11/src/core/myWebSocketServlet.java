package core;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.StringTokenizer;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.WsOutbound;

/**
 * Servlet implementation class myWebSocketServlet
 */
@WebServlet("/myWebSocketServlet")
public class myWebSocketServlet extends WebSocketServlet {
	 //private static final long serialVersionUID = 1L;
	@Override
	 public StreamInbound createWebSocketInbound( String protocol, HttpServletRequest request)
     {
         return new TheWebSocket( );
     }
     
	 private class TheWebSocket extends MessageInbound
     {
             private WsOutbound outbound;
 
             @Override
             public void onOpen( WsOutbound outbound )
             {
                 this.outbound = outbound;
             }
             @Override
             public void onTextMessage( CharBuffer buffer ) throws IOException
             {
                     
                     outbound.writeTextMessage(jumble(buffer));
             }
			@Override
			protected void onBinaryMessage(ByteBuffer arg0) throws IOException {
				// TODO Auto-generated method stub
				
			}
     }
	 public CharBuffer jumble(CharBuffer tmp){
		//System.out.println(tmp);
		StringTokenizer st = new StringTokenizer(tmp.toString());
		st.countTokens();
		if (st.countTokens()>=1){
			String st1 = st.nextToken();
			String toSend = "";
			if (st1.length() >2){
				 toSend = st1.substring(0,1) +", "+ st1.substring(0,1) + ", " + st1.substring(0,2) + ", "+tmp.toString()+"...";
			} else {
				 toSend = st1.substring(0,1) +", "+ st1.substring(0,1) + ", " + tmp.toString()+"...";
			}
			CharBuffer Sending = CharBuffer.wrap(toSend);
			return Sending;		
		} else {
		 return  tmp;
		}
	 }
 }

