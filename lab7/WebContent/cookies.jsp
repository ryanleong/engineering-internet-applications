<%
        // Create cookies for first and last names.      
        Cookie datetime = new Cookie("datetime",
                        (new java.util.Date() + ""));

        // Set expiry date after 24 Hrs for both the cookies.
        datetime.setMaxAge(60 * 60 * 24);

        // Add both the cookies in the response header.
        response.addCookie(datetime);

        String name = request.getParameter("name");
        String data = request.getParameter("data");
		//System.out.println(name);
		//System.out.println(data);
        if (name != null && data != null) {
                if (!name.equals("") && !data.equals("")){
        			Cookie cookieData = new Cookie(name, data);
                	cookieData.setMaxAge(60 * 60 * 24);
                	response.addCookie(cookieData);
                	//response.setIntHeader("Refresh", 0);
                	String redirectURL = "/lab7?name=" + name + "&data=" + data;
            		response.sendRedirect(redirectURL);
                } else {
                	String redirectURL = "/lab7";
            		response.sendRedirect(redirectURL);
                }
        }
%>