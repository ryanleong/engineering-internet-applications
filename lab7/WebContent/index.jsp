<%
        // Create cookies for first and last names.      
        Cookie datetime = new Cookie("datetime",
                        (new java.util.Date() + ""));

        // Set expiry date after 24 Hrs for both the cookies.
        datetime.setMaxAge(60 * 60 * 24);

        // Add both the cookies in the response header.
        response.addCookie(datetime);

%>
<html>
<head>
<title>Cookies</title>
</head>
<body>
        <center>
                <h1>Setting Cookies</h1>
        </center>

        <form id="cookieForm" action="cookies.jsp">
                <input type="text" name="name" placeholder="Name"> <br />
                <input type="text" name="data" placeholder="Value"> <br /> 
                <input type="submit" value="Submit" />
        </form>

        <div id="content">
        
                <%
                        Cookie cookie = null;
                        Cookie[] cookies = null;
                        // Get an array of Cookies associated with this domain
                        cookies = request.getCookies();
                        if (cookies != null) {
                                
                                
                                String outputString = "";
                                String time = "<b>Time of last visit</b>: ";
                                
                                for (int i = 0; i < cookies.length; i++) {
                                        cookie = cookies[i];
                                        
                                        if(cookie.getName().equals("datetime")) {
                                                time = time + cookie.getValue();
                                        }
                                        else if (!cookie.getName().equals("JSESSIONID")) {
                                                outputString = outputString + "<tr><td>" + cookie.getName() + "</td>";
                                                outputString = outputString + "<td>" + cookie.getValue() + "</td></tr>";
                                        }
                                                
                                }
                                
                                out.println(time + "<br /><br />");
                                
                                out.println("<b>Cookies in browser</b><br /><br />");
                                
                                if(outputString.equals("")) {
                                        out.println("No cookies in browser.");
                                }
                                else {
                                        out.print("<table width='200'><tr><td><b>Name</b></td><td><b>Value</b></td></tr>");
                                        out.print(outputString + "</table>");
                                }

                        } else {
                                out.println("No cookies in browser.");
                        }
                %>
                
        </div>

</body>
</html>