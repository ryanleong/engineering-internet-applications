// Keep track of messages
index = 0;

$(document).ready(
	function() {
		
		// Hide dialog box on load
	    $("#dialog").dialog({
	        autoOpen: false,
	        modal: true
	    });
		
		// style for button
		$( "input[type=submit], button" ).button();
		
		// Share button
		$('#share').click(function(e) {
			e.preventDefault();
			
			// Display dialog on click
			$( "#dialog" ).dialog("open");
				
		});
		
		$( "#emailform" ).submit(function() {
			var chatURL = "url=" + document.URL + "&email=" + $('#email').val();
			
			$.ajax({
				type : "post",
				url : "EmailServlet",
				data : chatURL,
				success : function(msg) {
					
					alert(msg);
					
				}
			});
			
			return false;
		});
		
		$('#chatform').submit(function() {
			var name = $('#name').val();
			var message = $('#message').val();
			var datastr = 'name=' + name +'&message='+ message;
			
			if(message != "" && name != "") {
				$.ajax({
					dataType : 'json', 
					type : "post",
					url : "ChatServlet",
					data : datastr,
					success : function(msg) {
						
						appendToChat(msg);
						
						// increase index
						index = index + 1;
						
						// Clear input box
						$("#message").val("");

						// Set focus
						$("#message").focus();

						// Scroll to bottom
						$("#chat").scrollTop($("#chat")[0].scrollHeight);
					}
				});
			}
			

			
			return false;
		});
		
		
		// Resize window dynamically
		$(window).resize(
			function() {
				$("#chat").height(
						$(window).height() - $("#header").height() - $("#footer").height() - $("#rj-credits").height() - 50);
	
		});
		
		doPoll();
});

function doPoll(){
	var current = "index=" + index;
	
	// initialisation
	$.ajax({
		dataType: "json",
		type : "get",
		url : "ChatServlet",
		data : current,
		success : function(msg) {
			
			$.each(msg, function(i, post){
//				var input = "<span class=\"message\">" + post.message + "</span><br/><br/><br/>";
//				
//				input += "<span class=\"chatdetails\">" + post.datetime + " - " + post.name + "</span><br/><br/>";
//				
//				$("#chat").append(input);
				
				appendToChat(post);
				
				index = post.index;
				
				// Scroll to bottom
				$("#chat").scrollTop($("#chat")[0].scrollHeight);
			});

		}
	});
	
	setTimeout(doPoll, 500);
}

function appendToChat(post) {
	var input = "<span class=\"message\">" + post.message + "</span><br/><br/><br/>";
	
	input += "<span class=\"chatdetails\">" + post.datetime + " - " + post.name + "</span><br/><br/>";
	
	$("#chat").append(input);
}


